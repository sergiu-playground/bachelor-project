package com.finalproject.domain.dto;

import java.io.Serializable;
import java.util.Date;

public class UserRegistrationDto implements Serializable {

    private String email;
    private String password;
    private String confirmPassword;
    private String firstName;
    private String lastName;
    private Date birthday;
    private String year;
    private String semester;
    private String group;
    private String registrationType;

    public UserRegistrationDto() {
    }

    public UserRegistrationDto(String email, String password, String confirmPassword,
                               String firstName, String lastName, Date birthday) {
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
    }

    public UserRegistrationDto(String email, String password, String confirmPassword, String firstName, String lastName, Date birthday, String year, String semester, String group, String registrationType) {
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.year = year;
        this.semester = semester;
        this.group = group;
        this.registrationType = registrationType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getRegistrationType() {
        return registrationType;
    }

    public void setRegistrationType(String registrationType) {
        this.registrationType = registrationType;
    }
}
