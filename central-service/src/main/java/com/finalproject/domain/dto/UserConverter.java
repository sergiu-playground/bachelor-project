package com.finalproject.domain.dto;

import com.finalproject.domain.Student;
import com.finalproject.domain.Teacher;
import com.finalproject.domain.User;

public class UserConverter {

    public static User getFromUserRegistrationDTO(final UserRegistrationDto userRegistrationDto) {
        User newUser = new User();
        newUser.setEmail(userRegistrationDto.getEmail());
        newUser.setPassword(userRegistrationDto.getPassword());
        newUser.setFirstName(userRegistrationDto.getFirstName()+ " " + userRegistrationDto.getLastName());
        newUser.setBirthday(userRegistrationDto.getBirthday());

        return newUser;
    }

    public static Student getStudentFromUserRegistrationDTO(final UserRegistrationDto userRegistrationDto) {

        Student newStudent = new Student();
        newStudent.setEmail(userRegistrationDto.getEmail());
        newStudent.setPassword(userRegistrationDto.getPassword());
        newStudent.setFirstName(userRegistrationDto.getFirstName()+ " " + userRegistrationDto.getLastName());
        newStudent.setBirthday(userRegistrationDto.getBirthday());
        newStudent.setGroup(userRegistrationDto.getGroup());
        newStudent.setYear(userRegistrationDto.getYear());
        newStudent.setSemester(userRegistrationDto.getSemester());

        return newStudent;
    }

    public static Teacher getTeacherFromUserRegistrationDTO(final UserRegistrationDto userRegistrationDto) {

        Teacher newTeacher = new Teacher();
        newTeacher.setEmail(userRegistrationDto.getEmail());
        newTeacher.setPassword(userRegistrationDto.getPassword());
        newTeacher.setFirstName(userRegistrationDto.getFirstName()+ " " + userRegistrationDto.getLastName());
        newTeacher.setBirthday(userRegistrationDto.getBirthday());
        newTeacher.setYear(userRegistrationDto.getYear());
        newTeacher.setSemester(userRegistrationDto.getSemester());

        return newTeacher;
    }
}