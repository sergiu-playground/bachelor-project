package com.finalproject.domain.dto;

import java.io.Serializable;
import java.util.Objects;

public class TeacherDto implements Serializable {

    private String name;
    private String description;
    private String websitePrefix;
    private String website;
    private String areasOfInterest;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWebsitePrefix() {
        return websitePrefix;
    }

    public void setWebsitePrefix(String websitePrefix) {
        this.websitePrefix = websitePrefix;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAreasOfInterest() {
        return areasOfInterest;
    }

    public void setAreasOfInterest(String areasOfInterest) {
        this.areasOfInterest = areasOfInterest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeacherDto that = (TeacherDto) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(websitePrefix, that.websitePrefix) &&
                Objects.equals(website, that.website) &&
                Objects.equals(areasOfInterest, that.areasOfInterest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, websitePrefix, website, areasOfInterest);
    }
}
