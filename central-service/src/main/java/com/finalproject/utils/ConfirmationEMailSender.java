package com.finalproject.utils;

import com.finalproject.domain.Token;
import com.finalproject.domain.User;
import com.finalproject.repository.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@PropertySource("classpath:development.properties")
public class ConfirmationEMailSender {

    @Autowired
    private TokenRepository tokenRepository;

    @Value("${server-domain}")
    private String domain;

    @Value("${server-port}")
    private String port;

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendConfirmationMail(final User user) {
        String token = UUID.randomUUID().toString();
        tokenRepository.save(new Token(token, user));

        String recipientAddress = user.getEmail();
        String subject = "Account Confirmation";
        String message = "For confirming your email address please click on the next link: ";
        String confirmationURL = "/api/registration/registrationConfirm.html?token=" + token;

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(message + domain + port + confirmationURL);
        javaMailSender.send(email);

    }
}
