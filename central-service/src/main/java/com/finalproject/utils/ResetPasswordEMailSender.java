package com.finalproject.utils;

import com.finalproject.domain.Token;
import com.finalproject.domain.User;
import com.finalproject.repository.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@PropertySource("classpath:development.properties")
public class ResetPasswordEMailSender {

    @Value("${server-domain}")
    private String domain;

    @Value("${server-port}")
    private String port;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private TokenRepository tokenRepository;

    public void sendResetPasswordToken(final User user) {
        String token = UUID.randomUUID().toString();
        tokenRepository.save(new Token(token, user));

        String userEmail = user.getEmail();
        String subject = "Reset password";
        String message = "Link: ";
        String resetURI = "/api/registration/resetPasswordConfirm.html?id=" + user.getId() + "&token=" + token;

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(userEmail);
        email.setSubject(subject);
        email.setText(message + domain + port + resetURI);
        javaMailSender.send(email);

    }
}
