package com.finalproject.utils;

import org.springframework.web.multipart.support.StandardServletMultipartResolver;

import javax.servlet.http.HttpServletRequest;

public class PostAndPutCommonsMultipartResolver extends StandardServletMultipartResolver {

    private static final String POST_METHOD = "POST";
    private static final String PUT_METHOD = "PUT";
    private static final String PATCH_METHOD = "PATCH";

    @Override
    public boolean isMultipart(HttpServletRequest request) {

        if ((request != null) && (POST_METHOD.equalsIgnoreCase(request.getMethod()) || PUT_METHOD.equalsIgnoreCase(request.getMethod()) || PATCH_METHOD.equalsIgnoreCase(request.getMethod()))) {
            String contentType = request.getContentType();
            return (contentType != null && contentType.toLowerCase().startsWith("multipart/"));

        }
        return false;
    }
}