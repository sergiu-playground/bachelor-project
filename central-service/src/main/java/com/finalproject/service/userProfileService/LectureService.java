package com.finalproject.service.userProfileService;

import com.finalproject.service.userProfileService.dto.CourseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LectureService {

    @Autowired
    private Oauth2RestTemplateConfiguration oauth2RestTemplateConfiguration;

    public String getCoursesByLecture(String lectureName) {
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        String url = "http://localhost:9001/api/lecture/" + lectureName;
        String data = oAuth2RestTemplate.getForObject(url, String.class);
        return data;
    }

    public String getCoursesFromTeacherAndLecture(String teacherId, String lecture) {
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        String url = "http://localhost:9001/api/lecture/getCoursesBy/" + teacherId + "/" + lecture;
        String data = oAuth2RestTemplate.getForObject(url, String.class);
        return data;
    }

    public String findAll() {
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        String url = "http://localhost:9001/api/lecture/getAll";
        String data = oAuth2RestTemplate.getForObject(url, String.class);
        return data;
    }

    public String findLecturesByTeacherId(String teacherId) {
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        String url = "http://localhost:9001/api/lecture/getLecturesByTeacherId/" + teacherId;
        String data = oAuth2RestTemplate.getForObject(url, String.class);
        return data;
    }
}