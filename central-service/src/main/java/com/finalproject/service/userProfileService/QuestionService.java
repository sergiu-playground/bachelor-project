package com.finalproject.service.userProfileService;

import com.finalproject.domain.User;
import com.finalproject.service.UserService;
import com.finalproject.service.userProfileService.dto.QuestionDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class QuestionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuestionService.class);

    @Autowired
    private Oauth2RestTemplateConfiguration oauth2RestTemplateConfiguration;

    @Autowired
    private UserService userService;


    public String getActualStudentQuestions() {
        LOGGER.info("Trying to get questions for student");
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        String url = "http://localhost:9002/api/question/getAll";
        String data = oAuth2RestTemplate.getForObject(url, String.class);
        return data;
    }

    public String getQuestionsForTeacher(String teacherId) {
        LOGGER.info("Trying to get questions for the teacher with id = {}", teacherId);
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        String url = "http://localhost:9002/api/question/2";
        String data = oAuth2RestTemplate.getForObject(url, String.class);
        return data;
    }

    public String saveQuestion(QuestionDTO question) {
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        User user = userService.findByName(question.getTeacherName()).get(0);
        question.setAnswererId(user.getId().toString());
        question.setPostingDate(Calendar.getInstance().getTime());
        String url = "http://localhost:9002/api/question/save";
        String data = oAuth2RestTemplate.postForObject(url, question, String.class);
        return data;
    }

    public String unansweredQuestions() {
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        ResourceOwnerPasswordResourceDetails resource = (ResourceOwnerPasswordResourceDetails) oAuth2RestTemplate.getResource();
        String email = resource.getUsername();
        Long id = userService.getUserByEmail(email).getId();
        String url = "http://localhost:9002/api/question/" + id;
        LOGGER.info("Trying to get unanswered questions from teacher with id = {}", id);
        String data = oAuth2RestTemplate.getForObject(url, String.class);
        return data;
    }

    public void answerQuestion(QuestionDTO question) {
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        String url = "http://localhost:9002/api/question/answerQuestion";
        oAuth2RestTemplate.postForObject(url, question, String.class);
    }

    public String findAllTeacherAnsweredQuestions() {
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        ResourceOwnerPasswordResourceDetails resource = (ResourceOwnerPasswordResourceDetails) oAuth2RestTemplate.getResource();
        String email = resource.getUsername();
        Long id = userService.getUserByEmail(email).getId();
        String url = "http://localhost:9002/api/question/getAll/" + id;
        LOGGER.info("Trying to get questions for the teacher with id = {}", id);
        String data = oAuth2RestTemplate.getForObject(url, String.class);
        return data;
    }

    public String getAnsweredQuestionsFromTeacher(String teacherId) {
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        String url = "http://localhost:9002/api/question/getAll/" + teacherId;
        LOGGER.info("Trying to get answered questions from teacher with id = {}", teacherId);
        String data = oAuth2RestTemplate.getForObject(url, String.class);
        return data;
    }
}