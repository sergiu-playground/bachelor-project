package com.finalproject.service.userProfileService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;

@Service
public class NewsFeedService {


    @Autowired
    private Oauth2RestTemplateConfiguration oauth2RestTemplateConfiguration;

    public String getData() {
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        String url = "http://localhost:9002/api/newsFeed/";
        String data = oAuth2RestTemplate.getForObject(url, String.class);
        return data;
    }

}