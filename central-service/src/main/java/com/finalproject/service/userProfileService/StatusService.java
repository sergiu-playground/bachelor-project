package com.finalproject.service.userProfileService;

import com.finalproject.domain.User;
import com.finalproject.service.UserService;
import com.finalproject.service.userProfileService.dto.StatusDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class StatusService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StatusService.class);

    @Autowired
    private Oauth2RestTemplateConfiguration oauth2RestTemplateConfiguration;

    @Autowired
    private UserService userService;

    public String getAllStatusesFromTeachers() {
        LOGGER.info("Trying to get statuses from teachers");
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        String url = "http://localhost:9002/api/status/getAll";
        String data = oAuth2RestTemplate.getForObject(url, String.class);
        return data;
    }

    public String addStatus(StatusDTO status) {
        LOGGER.info("Trying to add status from teacher ={}", status.getUserId());
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        User teacher = userService.findById(status.getUserId());
        status.setName(teacher.getFirstName());
        status.setPostingDate(Calendar.getInstance().getTime());
        String url = "http://localhost:9002/api/status/save";
        String data = oAuth2RestTemplate.postForObject(url, status, String.class);
        return data;
    }

    public String getAllTeacherStatuses() {
        LOGGER.info("Trying to get statuses from teachers");
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        ResourceOwnerPasswordResourceDetails resource = (ResourceOwnerPasswordResourceDetails) oAuth2RestTemplate.getResource();
        String email = resource.getUsername();
        Long id = userService.getUserByEmail(email).getId();
        String url = "http://localhost:9002/api/status/" + id;
        String data = oAuth2RestTemplate.getForObject(url, String.class);
        return data;
    }
}
