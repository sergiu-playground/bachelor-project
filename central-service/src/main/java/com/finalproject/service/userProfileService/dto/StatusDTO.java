package com.finalproject.service.userProfileService.dto;

import java.io.Serializable;
import java.util.Date;

public class StatusDTO implements Serializable {

    private String description;
    private Date postingDate;
    private String userId;
    private String name;

    public StatusDTO() {
    }

    public StatusDTO(String description, Date postingDate) {
        this.description = description;
        this.postingDate = postingDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(Date postingDate) {
        this.postingDate = postingDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
