package com.finalproject.service.userProfileService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.security.oauth2.common.AuthenticationScheme;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;

@Component
@PropertySource({"classpath:authorizationServer.properties"})
public class Oauth2RestTemplateConfiguration {

    @Value("${client.id}")
    private String clientId;

    @Value("${client.secret}")
    private String clientSecret;

    @Value("#{'${client.scope}'.split(',')}")
    private String[] scopes;

    @Value("${access.token.uri}")
    private String accessTokenUri;

    private OAuth2ProtectedResourceDetails resource() {

        ResourceOwnerPasswordResourceDetails resourceDetails = new ResourceOwnerPasswordResourceDetails();
        resourceDetails.setClientAuthenticationScheme(AuthenticationScheme.header);
        resourceDetails.setAccessTokenUri(accessTokenUri);
        resourceDetails.setScope(new ArrayList<>(Arrays.asList(scopes)));
        resourceDetails.setClientId(clientId);
        resourceDetails.setClientSecret(clientSecret);

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        resourceDetails.setUsername(userDetails.getUsername());
        resourceDetails.setPassword(userDetails.getPassword());


        return resourceDetails;
    }

    public OAuth2RestOperations getNewOAuth2RestTemplate() {

        return new OAuth2RestTemplate(resource());
    }
}