package com.finalproject.service.userProfileService;

import com.finalproject.domain.User;
import com.finalproject.service.UserService;
import com.finalproject.service.userProfileService.dto.CourseDTO;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class CourseService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuestionService.class);

    @Autowired
    private Oauth2RestTemplateConfiguration oauth2RestTemplateConfiguration;

    @Autowired
    private UserService userService;

    public String saveCourse(String courseName, String lectureName, MultipartFile file) {
        LOGGER.debug("saveCourse with course name {} and lecture name {}", courseName, lectureName);
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        ResourceOwnerPasswordResourceDetails resource = (ResourceOwnerPasswordResourceDetails) oAuth2RestTemplate.getResource();
        String email = resource.getUsername();
        String url = "http://localhost:9001/api/course/save/";
        HttpEntity<MultiValueMap<String, Object>> requestEntity = createRequest(courseName, lectureName, file, email);

        String requestData = oAuth2RestTemplate.postForObject(url, requestEntity, String.class);
        LOGGER.info("Course {} was saved successfully.", courseName);
        return requestData;
    }

    private HttpEntity<MultiValueMap<String, Object>> createRequest(String courseName, String lectureName, MultipartFile file, String email) {
        User user = userService.getUserByEmail(email);
        MultiValueMap<String, Object> request = new LinkedMultiValueMap<>();
        CourseDTO courseDTO = new CourseDTO();
        courseDTO.setCourseName(courseName);
        courseDTO.setLectureName(lectureName);
        courseDTO.setTeacherId(user.getId().toString());
        String contentsAsResource = null;
        String contentType = null;
        if(file != null) {
            try {
                contentsAsResource = Base64.encodeBase64String(file.getBytes());
                contentType = file.getContentType();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        request.add("courseDto", courseDTO);
        request.add("file", contentsAsResource);
        request.add("contentType", contentType);

        return new HttpEntity<>(request);
    }

    public String getTeacherCourses() {
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        ResourceOwnerPasswordResourceDetails resource = (ResourceOwnerPasswordResourceDetails) oAuth2RestTemplate.getResource();
        String email = resource.getUsername();
        LOGGER.debug("getTeacherCourses: Get All courses for teacher: {}", email);
        User user = userService.getUserByEmail(email);
        String url = "http://localhost:9001/api/course/getTeacherCourses/" + user.getId().toString();

        return oAuth2RestTemplate.getForObject(url, String.class);
    }

    public CourseDTO getCourseContent(String id) {
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        LOGGER.debug("getCourseContent: Get course with id: {}", id);
        String url = "http://localhost:9001/api/course/" + id;

        return oAuth2RestTemplate.getForObject(url, CourseDTO.class);
    }

    public String getTeacherCoursesByTeacherId(String teacherId) {
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        LOGGER.debug("getTeacherCourses: Get All courses for teacher: {}", teacherId);
        String url = "http://localhost:9001/api/course/getTeacherCourses/" + teacherId;

        return oAuth2RestTemplate.getForObject(url, String.class);
    }

    public void update(String courseName, String lectureName, MultipartFile file, String courseId) {
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        ResourceOwnerPasswordResourceDetails resource = (ResourceOwnerPasswordResourceDetails) oAuth2RestTemplate.getResource();
        String email = resource.getUsername();
        String url = "http://localhost:9001/api/course/" + courseId;
        HttpEntity<MultiValueMap<String, Object>> requestEntity = createRequest(courseName, lectureName, file, email);

        oAuth2RestTemplate.put(url, requestEntity);
    }
}
