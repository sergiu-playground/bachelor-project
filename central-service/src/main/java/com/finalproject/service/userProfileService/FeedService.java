package com.finalproject.service.userProfileService;

import com.finalproject.domain.User;
import com.finalproject.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.stereotype.Service;

@Service
public class FeedService {

    @Autowired
    private Oauth2RestTemplateConfiguration oauth2RestTemplateConfiguration;

    @Autowired
    private UserService userService;

    public String getFeedForTeacher() {
        OAuth2RestTemplate oAuth2RestTemplate = (OAuth2RestTemplate) oauth2RestTemplateConfiguration.getNewOAuth2RestTemplate();
        ResourceOwnerPasswordResourceDetails resource = (ResourceOwnerPasswordResourceDetails) oAuth2RestTemplate.getResource();
        String email = resource.getUsername();
        User user = userService.getUserByEmail(email);
        String url = "http://localhost:9002/api/feed/getFeedForTeacher/" + user.getId();
        String data = oAuth2RestTemplate.getForObject(url, String.class);
        return data;
    }
}
