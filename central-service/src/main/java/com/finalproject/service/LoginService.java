package com.finalproject.service;

import com.finalproject.domain.User;
import com.finalproject.domain.dto.UserLoginDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.file.AccessDeniedException;

public interface LoginService {

    User login(final UserLoginDto userLoginDto) throws AccessDeniedException;

    void logout(HttpServletRequest request, HttpServletResponse response);
}
