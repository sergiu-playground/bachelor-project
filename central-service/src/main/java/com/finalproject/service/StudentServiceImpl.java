package com.finalproject.service;

import com.finalproject.domain.Role;
import com.finalproject.domain.Student;
import com.finalproject.domain.dto.UserRegistrationDto;
import com.finalproject.repository.RoleRepository;
import com.finalproject.repository.StudentRepository;
import com.finalproject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

import static com.finalproject.domain.dto.UserConverter.getStudentFromUserRegistrationDTO;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Override
    public Student create(UserRegistrationDto userRegistrationDto) {
        Student student = getStudentFromUserRegistrationDTO(userRegistrationDto);

        if (roleRepository.findByCode("ROLE_STUDENT") == null) {
            Role role = new Role();
            role.setCode("ROLE_STUDENT");
            roleRepository.save(role);
        }
        Role role = roleRepository.findByCode("ROLE_STUDENT");
        Set<Role> roles = new HashSet<>();
        roles.add(role);

        student.setRoles(roles);
        student.setPassword(passwordEncoder.encode(student.getPassword()));
        return save(student);
    }

    @Override
    public Student save(Student student) {
        Student savedStudent = studentRepository.save(student);
        return savedStudent;
    }
}
