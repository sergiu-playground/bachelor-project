package com.finalproject.service;

import com.finalproject.domain.User;
import com.finalproject.domain.dto.PasswordDto;
import com.finalproject.domain.dto.UserRegistrationDto;
import com.finalproject.exception.UserException;
import com.finalproject.exception.VerificationTokenException;

public interface RegistrationService {

    User registration(final UserRegistrationDto userRegistrationDto) throws UserException;

    User confirmUserAccount(final String token) throws VerificationTokenException;

    void changePassword(PasswordDto passwordDto) throws Exception;

    void forgotPassword(final String email) throws UserException;

    boolean confirmResetPasswordToken(final Long id, final String token) throws VerificationTokenException;
}
