package com.finalproject.service;

import com.finalproject.domain.Student;
import com.finalproject.domain.dto.UserRegistrationDto;

public interface StudentService {

    Student create(final UserRegistrationDto userRegistrationDto);

    Student save(final Student student);
}
