package com.finalproject.service;

import com.finalproject.domain.*;
import com.finalproject.domain.dto.UserRegistrationDto;
import com.finalproject.repository.RoleRepository;
import com.finalproject.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.finalproject.domain.dto.UserConverter.getFromUserRegistrationDTO;

/***
 * Service for handling {@link User}s and authentication.
 */
@Service
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserService{

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /***
     *  Creates a new User.
     *
     * @param userRegistrationDto User registration data transfer object.
     * @return created {@link User}
     */
    public User create(final UserRegistrationDto userRegistrationDto) {
        LOGGER.info("Trying to create user with email={}", userRegistrationDto.getEmail());
        User user = getFromUserRegistrationDTO(userRegistrationDto);
        if(userRepository.findAll().isEmpty()){
            Role role = new Role();
            roleRepository.save(role);
        }
        Role role = roleRepository.findByCode("ROLE_USER");
        Set<Role> roles = new HashSet<>();
        roles.add(role);

        user.setRoles(roles);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        LOGGER.info("Successfully create user with email={}", userRegistrationDto.getEmail());
        return save(user);
    }

    /***
     *  Save User in the database.
     *
     * @param user User to save
     * @return saved {@link User}
     */
    public User save(final User user) {
        LOGGER.info("Trying to save user with email={}", user.getEmail());
        User saveUser = userRepository.save(user);
        LOGGER.info("Successfully saved user with email={}", user.getEmail());
        return saveUser;
    }

    /***
     *  Search for a user in database.
     *
     * @param email User email
     * @return a {@link User} if found or <code>null</code> otherwise
     */
    public User getUserByEmail(final String email) {
        return userRepository.findByEmail(email);
    }

    public List<User> findByName(String teacherName) {
        return userRepository.findByFirstName(teacherName);
    }

    public User findById(String userId) {

        return userRepository.findOne(Long.valueOf(userId));
    }
}