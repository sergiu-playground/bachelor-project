package com.finalproject.service;

import com.finalproject.domain.User;
import com.finalproject.domain.dto.UserLoginDto;
import com.finalproject.security.CustomUserDetailsService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.jaas.SecurityContextLoginModule;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.file.AccessDeniedException;

@Service
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginServiceImpl implements LoginService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginServiceImpl.class);

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    private UserService userService;

    /***
     *  Authenticate a user.
     *
     * @param userLoginDto User login Data transfer object.
     * @throws AccessDeniedException If the user doesn't confirm the account or email or password is incorrect.
     */
    @Override
    public User login(final UserLoginDto userLoginDto) throws AccessDeniedException {
        LOGGER.info("Trying to authenticate user with email={}", userLoginDto.getEmail());

        UserDetails userDetails = validateUser(userLoginDto);

        if (userDetails != null) {
            SecurityContext securityContext = SecurityContextHolder.getContext();
            Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            securityContext.setAuthentication(authentication);

            LOGGER.info("Successfully authenticate user with email={}", userLoginDto.getEmail());
            User user = userService.getUserByEmail(userDetails.getUsername());
            return  user;
        } else {
            LOGGER.warn("Failed to authenticate user with email={}", userLoginDto.getEmail());
            throw new AccessDeniedException("Incorrect email or password");
        }
    }

    /***
     *
     *  Trying to validate user credentials.
     *
     * @param userLoginDto User login Data Transfer Object.
     * @return valid {@link UserDetails} if the user credentials are good, null otherwise
     * @throws AccessDeniedException if the user wasn't found in database or he doesn't confirm the account
     */
    private UserDetails validateUser(final UserLoginDto userLoginDto) throws AccessDeniedException {
        LOGGER.info("Trying to validate the user with email={}", userLoginDto.getEmail());
        UserDetails user = customUserDetailsService.loadUserByUsername(userLoginDto.getEmail());

        if(user == null)
        {
            LOGGER.warn("This account doesn't exist.");
            throw new AccessDeniedException("This account doesn't exist.");
        }

        if (!user.isEnabled()) {
            LOGGER.warn("The account isn't confirmed, user with email={}", userLoginDto.getEmail());
            throw new AccessDeniedException("The account isn't confirmed.");
        }
        boolean isPasswordCorrect = passwordEncoder.matches(userLoginDto.getPassword(), user.getPassword());

        if (isPasswordCorrect) {
            LOGGER.info("Successfully validate the user with email={}", userLoginDto.getEmail());
            return user;
        }
        return null;
    }

    /**
     * Logout a user.
     *
     * @param request
     * @param response
     */
    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.info("Trying to logout user");
        SecurityContextHolder.clearContext();
        LOGGER.info("Successfully logout user");
    }
}