package com.finalproject.service;

import com.finalproject.domain.Teacher;
import com.finalproject.domain.dto.TeacherDto;
import com.finalproject.domain.dto.UserRegistrationDto;

import java.util.List;

public interface TeacherService {

    Teacher create(UserRegistrationDto userRegistrationDto);

    Teacher save(Teacher teacher);

    List<Teacher> getAll();

    void update(Teacher teacher);

    TeacherDto findOne(String email);

    TeacherDto findById(Long id);
}
