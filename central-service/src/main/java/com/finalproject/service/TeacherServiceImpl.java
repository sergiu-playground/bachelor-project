package com.finalproject.service;

import com.finalproject.domain.Role;
import com.finalproject.domain.Teacher;
import com.finalproject.domain.dto.TeacherDto;
import com.finalproject.domain.dto.UserLoginDto;
import com.finalproject.domain.dto.UserRegistrationDto;
import com.finalproject.repository.RoleRepository;
import com.finalproject.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.finalproject.domain.dto.UserConverter.getTeacherFromUserRegistrationDTO;

@Service
@Transactional
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Override
    public Teacher create(UserRegistrationDto userRegistrationDto) {
        Teacher teacher = getTeacherFromUserRegistrationDTO(userRegistrationDto);

        if (roleRepository.findByCode("ROLE_TEACHER") == null) {
            Role role = new Role();
            role.setCode("ROLE_TEACHER");
            roleRepository.save(role);
        }
        Role role = roleRepository.findByCode("ROLE_TEACHER");
        Set<Role> roles = new HashSet<>();
        roles.add(role);

        teacher.setRoles(roles);
        teacher.setPassword(passwordEncoder.encode(teacher.getPassword()));
        return save(teacher);
    }

    @Override
    public Teacher save(Teacher teacher) {
        Teacher savedTeacher = teacherRepository.save(teacher);
        return savedTeacher;
    }

    @Override
    public List<Teacher> getAll() {
        List<Teacher> teachers = teacherRepository.findAll();
        return teachers;
    }

    @Override
    public void update(Teacher teacher) {
        // get the id for email
        Teacher theTeacher = teacherRepository.findByEmail(teacher.getEmail());
        // set de id
        teacher.setId(theTeacher.getId());
        teacher.setPassword(theTeacher.getPassword());
        teacher.setBirthday(theTeacher.getBirthday());
        teacher.setEnabled(theTeacher.isEnabled());
        teacher.setRoles(theTeacher.getRoles());
        // update the teacher
        teacherRepository.save(teacher);
    }

    @Override
    public TeacherDto findOne(String email) {
        Teacher theTeacher = teacherRepository.findByEmail(email);

        TeacherDto teacherDto = new TeacherDto();
        teacherDto.setAreasOfInterest(theTeacher.getAreasOfInterest());
        teacherDto.setDescription(theTeacher.getDescription());
        teacherDto.setName(theTeacher.getFirstName());
        teacherDto.setWebsite(theTeacher.getWebsite());

        return teacherDto;
    }

    @Override
    public TeacherDto findById(Long id) {
        Teacher theTeacher = teacherRepository.findOne(id);

        TeacherDto teacherDto = new TeacherDto();
        teacherDto.setAreasOfInterest(theTeacher.getAreasOfInterest());
        teacherDto.setDescription(theTeacher.getDescription());
        teacherDto.setName(theTeacher.getFirstName());
        teacherDto.setWebsite(theTeacher.getWebsite());

        return teacherDto;
    }
}
