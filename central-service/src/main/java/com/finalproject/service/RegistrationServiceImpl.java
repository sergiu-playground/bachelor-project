package com.finalproject.service;

import com.finalproject.domain.*;
import com.finalproject.domain.dto.PasswordDto;
import com.finalproject.domain.dto.UserRegistrationDto;
import com.finalproject.exception.UserException;
import com.finalproject.exception.VerificationTokenException;
import com.finalproject.repository.TokenRepository;
import com.finalproject.security.CustomUserDetailsService;
import com.finalproject.utils.ConfirmationEMailSender;
import com.finalproject.utils.ResetPasswordEMailSender;
import com.finalproject.validator.EmailValidator;
import com.finalproject.validator.PasswordValidator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Calendar;

/***
 *  Service for handling registration process and reset password.
 */
@Service
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RegistrationServiceImpl implements RegistrationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationServiceImpl.class);

    @Autowired
    private ConfirmationEMailSender confirmationEMailSender;

    @Autowired
    private EmailValidator emailValidator;

    @Autowired
    private PasswordValidator passwordValidator;

    @Autowired
    private UserService userService;

    @Autowired
    private StudentServiceImpl studentService;

    @Autowired
    private TeacherServiceImpl teacherService;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private ResetPasswordEMailSender resetPasswordEMailSender;

    @Autowired
    private CustomUserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /***
     *
     * Registration a new {@link User}
     *
     * @param userRegistrationDto User registration Data transfer object.
     * @return new {@link User}
     * @throws UserException If user credentials aren't good.
     */
    @Override
    public User registration(final UserRegistrationDto userRegistrationDto) throws UserException {
        LOGGER.info("Trying to register user with email = {} ", userRegistrationDto.getEmail());
        boolean isValid = validateUser(userRegistrationDto);

        if (isValid) {
            LOGGER.info("User with email={}", userRegistrationDto.getEmail() + " is valid.");
            if (StringUtils.equals(userRegistrationDto.getRegistrationType(), "Student")) {
                Student student = studentService.create(userRegistrationDto);
                confirmationEMailSender.sendConfirmationMail(student);
                LOGGER.info("Successfully registered student with email = {} ", userRegistrationDto.getEmail());
                return student;
            } else if (StringUtils.equals(userRegistrationDto.getRegistrationType(), "Teacher")) {
                Teacher teacher = teacherService.create(userRegistrationDto);
                confirmationEMailSender.sendConfirmationMail(teacher);
                LOGGER.info("Successfully registered teacher with email = {} ", userRegistrationDto.getEmail());
                return teacher;
            } else {
                User user = userService.create(userRegistrationDto);
                confirmationEMailSender.sendConfirmationMail(user);
                LOGGER.info("Successfully registered user with email = {} ", userRegistrationDto.getEmail());
                return user;
            }
        }
        return null;
    }

    /***
     *
     *  Enables the {@link User} account and deletes the confirmation token.
     *
     * @param token an UUID.
     * @throws VerificationTokenException If the token wasn't found in the database.
     */
    @Override
    public User confirmUserAccount(final String token) throws VerificationTokenException {
        LOGGER.info("Trying to confirm a user account with token={}", token);
        User user = validateToken(token);
        user.setEnabled(true);
        LOGGER.info("Successfully confirmed user with email={} and token={}", user.getEmail(), token);

        return userService.save(user);
    }

    /***
     *
     * Update {@link User} password.
     *
     * @param passwordDto Password Data transfer object.
     * @throws Exception If password and confirm password aren't equal or password format is invalid.
     */
    @Override
    public void changePassword(final PasswordDto passwordDto) throws Exception {
        LOGGER.info("Trying to change the password.");
        String validatePasswordDto = passwordValidator.validate(passwordDto.getPassword(),
                passwordDto.getConfirmPassword());

        if (validatePasswordDto == null) {
            User user = userService.getUserByEmail(((org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext()
                    .getAuthentication()
                    .getPrincipal()).getUsername());

            user.setPassword(passwordEncoder.encode(passwordDto.getPassword()));
            userService.save(user);
            LOGGER.info("Successfully changed password for the user with email={}", user.getEmail());
        } else {
            LOGGER.warn("changePassword: error={}", validatePasswordDto);
            throw new Exception(validatePasswordDto);
        }
    }

    /***
     *
     * Sends an email with a confirmation link if a request for reset password was made.
     *
     * @param email {@link User} email.
     * @throws UserException If the email doesn't exist.
     */
    @Override
    public void forgotPassword(final String email) throws UserException {
        LOGGER.info("forgotPassword: email={}", email);

        User existingUser = userService.getUserByEmail(email);
        if (existingUser == null) {
            throw new UserException("This email doesn't exist.");
        }
        resetPasswordEMailSender.sendResetPasswordToken(existingUser);
        LOGGER.info("forgotPassword: user email={}", existingUser.getEmail());
    }

    /***
     *
     *  Authenticates a user so he can change his password and deletes the confirmation token.
     *
     * @param id {@link User} id.
     * @param token an UUID.
     * @return true if the token exists and it is not expired
     * @throws VerificationTokenException If the token wasn't found or the confirmation time has expired.
     */
    @Override
    public boolean confirmResetPasswordToken(final Long id, final String token) throws VerificationTokenException {
        LOGGER.info("confirmResetPasswordToken: user id={}, token={}", id, token);
        User user = validateToken(token);
        if (!user.getId().equals(id)) {
            throw new VerificationTokenException("This token wasn't find!");
        }

        UserDetails userDetails = userDetailsService.loadUserByUsername(user.getEmail());
        SecurityContextHolder.clearContext();
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        securityContext.setAuthentication(authentication);

        LOGGER.info("Success confirmResetPasswordToken: user email={}, token={}", user.getEmail(), token);
        return true;
    }

    /***
     *
     * Validate user token.
     *
     * @param token an UUID
     * @return {@link User} if the token exists and it is not expired
     * @throws VerificationTokenException if the token doesn't exists and it is expired
     */
    private User validateToken(final String token) throws VerificationTokenException {
        LOGGER.info("Trying to validate user token={}", token);
        Token verificationToken = tokenRepository.findByToken(token);
        if (verificationToken == null) {
            throw new VerificationTokenException("This token wasn't find!");
        }

        User user = verificationToken.getUser();
        Calendar calendar = Calendar.getInstance();
        if ((verificationToken.getExpirationDate().getTime() - calendar.getTime().getTime()) <= 0) {
            confirmationEMailSender.sendConfirmationMail(user);
            LOGGER.warn("Failed to set enabled a user with email = {}", user.getEmail());
            throw new VerificationTokenException("The confirmation time has expired, you will receive a new confirmation mail soon.");
        }
        tokenRepository.delete(verificationToken);
        return user;
    }

    /***
     *
     * Validate user credentials.
     *
     * @param userRegistrationDto User registration Data transfer object.
     * @return true if the credentials are good, throw an exception if not
     * @throws UserException
     */
    private boolean validateUser(final UserRegistrationDto userRegistrationDto) throws UserException {
        LOGGER.info("Trying to validate user  = {} ", userRegistrationDto.getEmail());
        UserDetails existingUserDetails = userDetailsService.loadUserByUsername(userRegistrationDto.getEmail());

        if (existingUserDetails != null) {
            throw new UserException("This email is already used.");
        }
        String emailValidate = emailValidator.validate(userRegistrationDto.getEmail());
        String passwordValidate = passwordValidator.validate(userRegistrationDto.getPassword(),
                userRegistrationDto.getConfirmPassword());
        if (emailValidate == null && passwordValidate == null) {
            return true;
        } else {
            LOGGER.warn("Failed to register a new user with email={}", userRegistrationDto.getEmail());
            if (emailValidate != null && passwordValidate != null) {
                throw new UserException(emailValidate + " " + passwordValidate);
            } else if (emailValidate != null) {
                throw new UserException(emailValidate);
            } else {
                throw new UserException(passwordValidate);
            }
        }
    }
}