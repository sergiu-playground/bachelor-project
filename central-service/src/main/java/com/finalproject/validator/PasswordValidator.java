package com.finalproject.validator;

import com.finalproject.domain.dto.*;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/***
 * Validator for {@link PasswordDto} and {@link UserRegistrationDto} password.
 */
@Component
public class PasswordValidator {

    private Pattern pattern;
    private Matcher matcher;

    private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";

    public PasswordValidator() {
        this.pattern = Pattern.compile(PASSWORD_PATTERN);
    }

    public String validate(final String password,final String confirmPassword) {
        if(!password.equals(confirmPassword)){
            return "Password does not match the confirm password.";
        }
        this.matcher = pattern.matcher(password);

        if (!matcher.matches()) {
            return "Password must contain at least 6 characters, one digit, one uppercase letter and one special symbol.";
        }
        return null;
    }
}