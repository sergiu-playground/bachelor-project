package com.finalproject.validator;

import com.finalproject.domain.dto.UserRegistrationDto;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/***
 * Validator for {@link UserRegistrationDto}.
 */

@Component
public class EmailValidator {

    private Pattern pattern;
    private Matcher matcher;

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public EmailValidator() {
        this.pattern = Pattern.compile(EMAIL_PATTERN);
    }

    public String validate(final String email) {
        this.matcher = pattern.matcher(email);

        if (!matcher.matches()) {
            return "The email format is invalid!";
        }
        return null;
    }
}