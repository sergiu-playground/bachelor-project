package com.finalproject.controller;

import com.finalproject.service.userProfileService.NewsFeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/oauth2")
public class OAuth2Controller {

    @Autowired
    private NewsFeedService newsFeedService;

    @RequestMapping(value = "/data", method = RequestMethod.GET)
    public ResponseEntity<String> getResourceServerData() {
        String data = newsFeedService.getData();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

}