package com.finalproject.controller.userProfile;

import com.finalproject.service.userProfileService.FeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/feed")
public class FeedController {

    @Autowired
    private FeedService feedService;

    @RequestMapping(value = "/getFeedForTeacher", method = RequestMethod.GET)
    public ResponseEntity<String> getFeedForTeacher() {
        String data = feedService.getFeedForTeacher();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }
}
