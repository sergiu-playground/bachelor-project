package com.finalproject.controller.userProfile;

import com.finalproject.service.userProfileService.CourseService;
import com.finalproject.service.userProfileService.dto.CourseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;

@RestController
@RequestMapping("/api/course")
public class CourseController {

    @Autowired
    private CourseService courseService;

    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<String> saveCourse(@RequestPart(value = "courseName") String courseName,
                                             @RequestPart(value = "lectureName") String lectureName,
                                             @RequestParam("file") MultipartFile file) {
        courseService.saveCourse(courseName, lectureName, file);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{courseId}", method = RequestMethod.PUT, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<String> update(@RequestPart(value = "courseName", required = false) String courseName,
                                             @RequestPart(value = "lectureName", required = false) String lectureName,
                                             @RequestParam(value = "file", required = false) MultipartFile file, @PathVariable(value = "courseId") String courseId) {

        courseService.update(courseName, lectureName, file, courseId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/getTeacherCourses", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getTeacherCourses() {
        String data = courseService.getTeacherCourses();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @RequestMapping(value = "/getTeacherCourses/{teacherId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getTeacherCoursesByTeacherId(@PathVariable(value = "teacherId")String teacherId) {
        String data = courseService.getTeacherCoursesByTeacherId(teacherId);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> getCourseContent(@PathVariable(value = "id") String id) {
        CourseDTO course = courseService.getCourseContent(id);

        byte[] responseDocument = course.getDocument();

        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.put("Content-Type", Collections.singletonList(course.getContentType()));

        return new ResponseEntity(responseDocument, headers, HttpStatus.OK);
    }
}