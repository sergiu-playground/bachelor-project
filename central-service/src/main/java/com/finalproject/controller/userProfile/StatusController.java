package com.finalproject.controller.userProfile;

import com.finalproject.service.userProfileService.NewsFeedService;
import com.finalproject.service.userProfileService.StatusService;
import com.finalproject.service.userProfileService.dto.StatusDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/status")
public class StatusController {

    @Autowired
    private StatusService statusService;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllStatusesFromTeachers() {

        String statuses = statusService.getAllStatusesFromTeachers();
        return new ResponseEntity<>(statuses, HttpStatus.OK);
    }

    @RequestMapping(value = "/saveStatus", method = RequestMethod.POST)
    public ResponseEntity<String> saveQuestion(@RequestBody StatusDTO status) {

        String data = statusService.addStatus(status);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @RequestMapping(value = "/getAllTeacherStatuses", method = RequestMethod.GET)
    public ResponseEntity<String> getAllTeacherStatuses() {
        String data = statusService.getAllTeacherStatuses();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }
}