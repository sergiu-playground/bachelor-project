package com.finalproject.controller.userProfile;

import com.finalproject.service.userProfileService.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/lecture")
public class LectureController {

    @Autowired
    private LectureService lectureService;

    @RequestMapping(value = "/{lecture}", method = RequestMethod.GET)
    public ResponseEntity getCoursesByLecture(@PathVariable("lecture") String lectureName) {
        String data = lectureService.getCoursesByLecture(lectureName);

        return new ResponseEntity(data, HttpStatus.OK);
    }

    @RequestMapping(value = "/getCoursesBy/{teacherId}/{lecture}", method = RequestMethod.GET)
    public ResponseEntity getCoursesFromATeacherAndLecture(@PathVariable("teacherId") String teacherId,
                                                           @PathVariable("lecture") String lecture) {

        String courses = lectureService.getCoursesFromTeacherAndLecture(teacherId, lecture);

        return new ResponseEntity(courses, HttpStatus.OK);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ResponseEntity getAllLectures(){
        String lectures = lectureService.findAll();

        return new ResponseEntity(lectures, HttpStatus.OK);
    }

    @RequestMapping(value = "/getLecturesByTeacherId/{teacherId}", method = RequestMethod.GET)
    public ResponseEntity getLecturesByTeacherId(@PathVariable("teacherId") String teacherId) {

        String lectures = lectureService.findLecturesByTeacherId(teacherId);

        return new ResponseEntity(lectures, HttpStatus.OK);
    }
}
