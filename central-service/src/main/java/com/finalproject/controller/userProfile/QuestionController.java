package com.finalproject.controller.userProfile;

import com.finalproject.service.userProfileService.Oauth2RestTemplateConfiguration;
import com.finalproject.service.userProfileService.QuestionService;
import com.finalproject.service.userProfileService.dto.QuestionDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/question")
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @Autowired
    private Oauth2RestTemplateConfiguration oauth2RestTemplateConfiguration;

    @RequestMapping(value = "/{teacherId}", method = RequestMethod.GET)
    public ResponseEntity<String> getResourceServerData(@PathVariable(name = "teacherId")String teacherId) {

        String data = questionService.getQuestionsForTeacher(teacherId);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @RequestMapping(value = "/forActualStudent", method = RequestMethod.GET)
    public ResponseEntity<String> getActualStuentQuestions() {

        String data = questionService.getActualStudentQuestions();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @RequestMapping(value = "/saveQuestion", method = RequestMethod.POST)
    public ResponseEntity<String> saveQuestion(@RequestBody QuestionDTO question) {

        String data = questionService.saveQuestion(question);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @RequestMapping(value = "/unansweredQuestions", method = RequestMethod.GET)
    public ResponseEntity<String> unansweredQuestions() {

        String data = questionService.unansweredQuestions();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @RequestMapping(value = "/answerQuestion", method = RequestMethod.POST)
    public ResponseEntity<?> answerQuestion(@RequestBody QuestionDTO question) {

        questionService.answerQuestion(question);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/teacherAnsweredQuestions", method = RequestMethod.GET)
    public ResponseEntity<String> teacherAnsweredQuestions() {

        String data = questionService.findAllTeacherAnsweredQuestions();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @RequestMapping(value = "/teacherAnsweredQuestions/{teacherId}", method = RequestMethod.GET)
    public ResponseEntity<?> answeredQuestionsFromTeacher(@PathVariable(value = "teacherId") String teacherId){
        String data = questionService.getAnsweredQuestionsFromTeacher(teacherId);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }


}
