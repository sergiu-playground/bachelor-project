package com.finalproject.controller.userProfile;

import com.finalproject.service.userProfileService.NewsFeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/newsFeed")
public class NewsFeedController {

    @Autowired
    private NewsFeedService newsFeedService;

    @RequestMapping(value = "/news", method = RequestMethod.GET)
    public ResponseEntity<String> getNewsfeed(){
        String data = newsFeedService.getData();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }
}
