package com.finalproject.controller;

import com.finalproject.domain.dto.EmptyJsonResponse;
import com.finalproject.domain.dto.UserRegistrationDto;
import com.finalproject.exception.UserException;
import com.finalproject.exception.VerificationTokenException;
import com.finalproject.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/registration")
public class RegistrationController {

    @Autowired
    @Qualifier(value = "registrationServiceImpl")
    private RegistrationService registrationService;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity registration(@RequestBody UserRegistrationDto userRegistrationDto) throws UserException {
        registrationService.registration(userRegistrationDto);
        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @RequestMapping(value = "/registrationConfirm", method = RequestMethod.GET)
    public ResponseEntity registrationConfirmEmail(@RequestParam("token") String token) throws VerificationTokenException {
        registrationService.confirmUserAccount(token);
        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }

}