package com.finalproject.controller;

import com.finalproject.domain.User;
import com.finalproject.domain.dto.EmptyJsonResponse;
import com.finalproject.domain.dto.UserLoginDto;
import com.finalproject.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.file.AccessDeniedException;

@RestController
@RequestMapping("/api")
public class LoginController {

    @Autowired
    @Qualifier(value = "loginServiceImpl")
    private LoginService loginService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity login(@RequestBody UserLoginDto userLoginDto) throws AccessDeniedException {
        User user = loginService.login(userLoginDto);
        User responseUser = new User();
        responseUser.setId(user.getId());
        responseUser.setRoles(user.getRoles());


        return new ResponseEntity(responseUser, HttpStatus.OK);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ResponseEntity logout(HttpServletRequest request, HttpServletResponse response){
        loginService.logout(request, response);

        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }
}