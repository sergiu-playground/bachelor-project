package com.finalproject.controller;

import com.finalproject.domain.dto.EmptyJsonResponse;
import com.finalproject.domain.dto.PasswordDto;
import com.finalproject.service.RegistrationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private RegistrationServiceImpl registrationServiceImpl;

    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    public ResponseEntity changePassword(@RequestBody PasswordDto passwordDto) throws Exception {
        registrationServiceImpl.changePassword(passwordDto);
        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }

}