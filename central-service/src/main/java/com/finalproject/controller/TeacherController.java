package com.finalproject.controller;

import com.finalproject.domain.Teacher;
import com.finalproject.domain.dto.EmptyJsonResponse;
import com.finalproject.domain.dto.TeacherDto;
import com.finalproject.service.TeacherService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/teacher")
public class TeacherController {

    @Autowired
    @Qualifier(value = "teacherServiceImpl")
    private TeacherService teacherService;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getAllTeachers() {
        List<Teacher> teacherList = teacherService.getAll();
        return new ResponseEntity<>(teacherList, HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity update(@RequestPart(value = "name", required = false) String name,
                                 @RequestPart(value = "description", required = false) String description,
                                 @RequestPart(value = "websitePrefix", required = false) String websitePrefix,
                                 @RequestPart(value = "website", required = false) String website,
                                 @RequestPart(value = "areasOfInterest", required = false) String areasOfInterest
    ) {
        Teacher newTeacher = populateTeacher(name, description, websitePrefix, website, areasOfInterest);
        teacherService.update(newTeacher);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/test", method = RequestMethod.PUT, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity logout(HttpServletRequest request, HttpServletResponse response) {

        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @RequestMapping(value = "/findOne", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity findOne() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        TeacherDto teacherList = teacherService.findOne(userDetails.getUsername());
        return new ResponseEntity<>(teacherList, HttpStatus.OK);
    }

    @RequestMapping(value = "/findById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity findById(@PathVariable(value = "id") String id) {
        TeacherDto teacher = teacherService.findById(Long.valueOf(id));
        return new ResponseEntity<>(teacher, HttpStatus.OK);
    }

    private Teacher populateTeacher(String name, String description, String websitePrefix, String website, String areasOfInterest) {
        Teacher result = new Teacher();

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        result.setEmail(userDetails.getUsername());

        if (StringUtils.isNotBlank(name)) {
            result.setFirstName(name);
        }
        if (StringUtils.isNotBlank(description)) {
            result.setDescription(description);
        }
        if (StringUtils.isNotBlank(website)) {
            result.setWebsite(websitePrefix + website);
        }
        if (StringUtils.isNotBlank(areasOfInterest)) {
            result.setAreasOfInterest(areasOfInterest);
        }
        return result;
    }

}