package com.finalproject.repository;

import com.finalproject.domain.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepository extends JpaRepository<Token, Long>{

    Token findByToken(String token);
}
