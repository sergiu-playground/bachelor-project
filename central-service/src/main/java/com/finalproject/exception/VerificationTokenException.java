package com.finalproject.exception;

public class VerificationTokenException extends Exception {

    public VerificationTokenException() {
    }

    public VerificationTokenException(String message) {
        super(message);
    }
}
