package com.finalproject.service;

import com.finalproject.domain.*;
import com.finalproject.domain.dto.UserLoginDto;
import com.finalproject.domain.dto.UserRegistrationDto;
import com.finalproject.repository.RoleRepository;
import com.finalproject.repository.UserRepository;
import com.finalproject.security.CustomUserDetailsService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.nio.file.AccessDeniedException;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.MockitoAnnotations.initMocks;

public class UserServiceTest {

    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private PasswordEncoder passwordEncoder;


    @Before
    public void setUp() throws Exception {
        initMocks(this);

        userService = spy(new UserService().builder()
                .userRepository(userRepository)
                .roleRepository(roleRepository)
                .passwordEncoder(passwordEncoder)
                .build()
        );
    }

    @Test
    public void shouldCreateAUser() throws Exception {
        UserRegistrationDto userDto = createUserDto();
        User user = createUser();

        doReturn(user).when(userRepository).save(any(User.class));

        User expected = user;
        User result = userService.create(userDto);

        assertEquals(result, expected);
    }

    private static User createUser() {
        Role role = new Role();
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        return new User("email", "password",roles);
    }

    private static UserRegistrationDto createUserDto()
    {
        return new UserRegistrationDto("email", "password", "password","firstname","lastname", Calendar.getInstance().getTime());
    }


}