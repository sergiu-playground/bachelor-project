package com.finalproject.service;

import com.finalproject.domain.dto.UserLoginDto;
import com.finalproject.security.CustomUserDetailsService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.MockitoAnnotations.initMocks;

public class LoginServiceImplTest {

    private LoginServiceImpl loginService;

    @Mock
    private CustomUserDetailsService userDetailsService;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Before
    public void setUp() throws Exception {
        initMocks(this);

        loginService = spy(new LoginServiceImpl().builder()
                .customUserDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder)
                .build()
        );
    }

    @Test
    @Ignore
    public void shouldLogin() throws Exception {
        UserLoginDto userDto = createUserLoginDto();
        UserDetails userDetails = createUserDetails();
        doReturn(userDetails).when(userDetailsService).loadUserByUsername(userDetails.getUsername());

        doReturn(true).when(passwordEncoder).matches(any(), any());
        loginService.login(userDto);

    }

    @Test(expected = AccessDeniedException.class)
    public void shouldFailLoginIfUserDoNotConfirmHisEmail() throws Exception {
        UserLoginDto userDto = createUserLoginDto();
        UserDetails userDetails = createUserDetails();
        doReturn(userDetails).when(userDetailsService).loadUserByUsername(userDetails.getUsername());
        loginService.login(userDto);
    }

    @Test(expected = AccessDeniedException.class)
    public void shouldFailLoginIfUserDoesNotExist() throws Exception {
        UserLoginDto userDto = createUserLoginDto();
        UserDetails userDetails = createUserDetails();
        doReturn(null).when(userDetailsService).loadUserByUsername(userDetails.getUsername());
        loginService.login(userDto);
    }


    private UserDetails createUserDetails() {
        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        return new org.springframework.security.core.userdetails.User(
                "email", "password", true,
                true,true,true,grantedAuthorities);
    }

    private UserLoginDto createUserLoginDto() {
        return new UserLoginDto("email", "password");
    }

}