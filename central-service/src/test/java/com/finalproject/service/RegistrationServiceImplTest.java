package com.finalproject.service;

import com.finalproject.domain.*;
import com.finalproject.domain.dto.PasswordDto;
import com.finalproject.domain.dto.UserRegistrationDto;
import com.finalproject.exception.UserException;
import com.finalproject.exception.VerificationTokenException;
import com.finalproject.repository.TokenRepository;
import com.finalproject.security.CustomUserDetailsService;
import com.finalproject.utils.ConfirmationEMailSender;
import com.finalproject.validator.EmailValidator;
import com.finalproject.validator.PasswordValidator;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class RegistrationServiceImplTest {

    public static final String PASSWORD = "password";
    public static final String INVALID_TOKEN = "invalid-token";
    public static final String TOKEN = "d8aedf32-98fc-4a33-9e7b-c60c79e4bb75";

    private RegistrationServiceImpl registrationServiceImpl;

    @Mock
    private EmailValidator emailValidator;

    @Mock
    private PasswordValidator passwordValidator;

    @Mock
    private TokenRepository tokenRepository;

    @Mock
    private UserService userService;

    @Mock
    private ConfirmationEMailSender confirmationEMailSender;

    @Mock
    private CustomUserDetailsService userDetailsService;

    @Before
    public void setUp() throws Exception {
        initMocks(this);


        this.registrationServiceImpl = spy(new RegistrationServiceImpl().builder()
                .userDetailsService(userDetailsService)
                .userService(userService)
                .tokenRepository(tokenRepository)
                .emailValidator(emailValidator)
                .passwordValidator(passwordValidator)
                .confirmationEMailSender(confirmationEMailSender)
                .build());
    }

    @Test(expected = UserException.class)
    public void shouldFailRegistrationIfEmailAlreadyExist() throws Exception {
        UserRegistrationDto userDto = createUserRegistrationDto();
        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        UserDetails user = new org.springframework.security.core.userdetails.User(
                "username", "password", grantedAuthorities);
        doReturn(user).when(userDetailsService).loadUserByUsername(userDto.getEmail());

        registrationServiceImpl.registration(userDto);
    }

    @Test
    public void shouldRegistrationIfCredentialsAreGood() throws Exception {
        UserRegistrationDto userDto = createUserRegistrationDto();
        User user = createUser();
        doReturn(null).when(emailValidator).validate(userDto.getEmail());
        doReturn(null).when(passwordValidator).validate(userDto.getPassword(), userDto.getConfirmPassword());
        doReturn(user).when(userService).create(any(UserRegistrationDto.class));
        doNothing().when(confirmationEMailSender).sendConfirmationMail(any(User.class));

        User resutlt = user;
        User expected = registrationServiceImpl.registration(userDto);

        assertEquals(resutlt, expected);
    }

    @Test(expected = UserException.class)
    public void shouldFailRegistrationIfCredentialsAreNotGood() throws Exception {
        UserRegistrationDto userDto = createUserRegistrationDto();
        doReturn("Invalid email.").when(emailValidator).validate(userDto.getEmail());
        doReturn("Invalid password").when(passwordValidator).validate(userDto.getPassword(), userDto.getConfirmPassword());

        registrationServiceImpl.registration(userDto);

    }

    @Test(expected = VerificationTokenException.class)
    public void shouldFailConfirmAccountIfTokenIsNotInDatabase() throws Exception {
        doReturn(null).when(tokenRepository).findByToken(anyString());
        registrationServiceImpl.confirmUserAccount(TOKEN);
    }

    @Test(expected = VerificationTokenException.class)
    public void shouldFailConfirmAccountIfTokenHasExpired() throws Exception {
        Token verificationToken = createWrongVerificationToken();
        doReturn(verificationToken).when(tokenRepository).findByToken(anyString());
        doNothing().when(confirmationEMailSender).sendConfirmationMail(any(User.class));
        registrationServiceImpl.confirmUserAccount(TOKEN);
    }

    @Test
    public void shouldConfirmAccountAndDeleteVerificationToken() throws Exception {
        Token verificationToken = createVerificationToken();
        doReturn(verificationToken).when(tokenRepository).findByToken(anyString());
        doNothing().when(tokenRepository).delete(any(Token.class));
        doReturn(verificationToken.getUser()).when(userService).save(any(User.class));

        User result = registrationServiceImpl.confirmUserAccount(TOKEN);

        assertEquals(result, verificationToken.getUser());
    }

    @Test(expected = Exception.class)
    public void shouldFailToChangePasswordIfPasswordAndConfirmPasswordAreDifferent() throws Exception {
        PasswordDto passwordDto = createPasswordDto();
        passwordDto.setConfirmPassword("confirm");

        registrationServiceImpl.changePassword(passwordDto);

    }

    @Test(expected = UserException.class)
    public void shouldFailToSendResetPasswordEmailIfEmailIsNotInDatabase() throws Exception {
        doReturn(null).when(userService).getUserByEmail(any(String.class));
        registrationServiceImpl.forgotPassword("user@1234.com");
    }

    @Test(expected = VerificationTokenException.class)
    public void shouldFailConfirmPasswordTokenIfTokenIsNotInDatabase() throws Exception {
        doReturn(null).when(tokenRepository).findByToken(any(String.class));
        registrationServiceImpl.confirmResetPasswordToken(1L, INVALID_TOKEN);
    }

    @Test(expected = VerificationTokenException.class)
    public void shouldFailConfirmPasswordTokenIfTokenIsExpired() throws Exception {
        Token token = createResetPasswordToken();
        doReturn(token).when(tokenRepository).findByToken(any(String.class));
        registrationServiceImpl.confirmResetPasswordToken(1L, TOKEN);
    }

    private Token createResetPasswordToken() {
        Date date = getLateDate();
        return new Token(1L, TOKEN, createUser(), date);
    }

    private User createUser() {
        Role role = new Role();
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        return new User(1L, "email", PASSWORD, roles);
    }

    private static PasswordDto createPasswordDto() {
        return new PasswordDto(PASSWORD, PASSWORD);
    }

    private UserRegistrationDto createUserRegistrationDto() {
        return new UserRegistrationDto("email", "password", "password","firstname","lastname", Calendar.getInstance().getTime());
    }

    private Token createWrongVerificationToken() {
        Date date = getLateDate();
        return new Token(1L, TOKEN, createUser(), date);
    }

    private Token createVerificationToken() {
        return new Token(TOKEN, createUser());
    }

    private Date getLateDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date date = null;
        try {
            date = sdf.parse("21/12/2012");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}