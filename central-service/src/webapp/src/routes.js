import React from 'react'
import { Switch, Route } from 'react-router';
import Login from './scenes/Sign/scenes/Login/Login'
import Register from "./scenes/Sign/scenes/Register/Register";
import NewsFeed from "./scenes/Home/News_feed/NewsFeed";
import Teachers from "./scenes/Home/News_feed/Menu/Teachers";
import UserProfilePage from './scenes/Home/User_profile/UserProfilePage';
import UserProfile from './scenes/Profile/UserProfile';
import CourseContent from './scenes/Profile/TeacherProf/teaching/CourseContent';
import TeacherContent from './scenes/Home/News_feed/Menu/Teachers/TeacherContent';


const Routes = () => (
    <Switch>
        <Route exact path='/' component={Login}/>
        <Route exact path='/registration' component={Register}/>
        <Route exact path='/newsFeed' component={NewsFeed}/>
        <Route exact path='/teachers' component={Teachers}/>
        <Route exact path='/home' component={UserProfilePage}/>
        <Route exact path='/userProfile' component={UserProfile}/>
        <Route path='/course/:id' component={CourseContent}/>
        <Route path='/teachers/:teacherId' component={TeacherContent}/>
    </Switch>
);

export default Routes;
