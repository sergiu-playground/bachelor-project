import { ACTIONS } from "../actions/studentActions";

const initialState = {
    data: [],
    isLoading: false,
    hasError: false,
    errorMessage: '',
    error: '',
    statusCode: 0
};

export default function studentReducer(state = initialState, action) {

    switch (action.type) {
        case ACTIONS.SAVE_QUESTION:
            return {
                ...state,
                isLoading: true,
            };
        case ACTIONS.SAVE_QUESTION_SUCCESS:
            return {
                ...state,
                hasError:false,
                isLoading:false,
                statusCode: action.payload.status,
            };
        case ACTIONS.SAVE_QUESTION_FAIL:
            return {
                ...state,
                isLoading: false,
                hasError: true,
                errorMessage: action.error.data,
                error: action.error.data
            };
        case ACTIONS.SAVE_QUESTION_RESOLVED_ERROR:
            return {
                ...state,
                isLoading: false,
                hasError: false,
                errorMessage: '',
                error: '',
            };
        default:
            return state;
    }
}