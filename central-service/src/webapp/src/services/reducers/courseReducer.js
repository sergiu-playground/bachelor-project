import { ACTIONS } from '../actions/courseActions';

const initialState = {
    teacherCourses: [],
    isLoading: false,
    waitingForSavingCourse: false,
    hasError: false,
    errorMessage: '',
    error: '',
    statusCode: 0
}

export default function courseReducer(state = initialState, action) {

    switch (action.type) {
        case ACTIONS.COURSE_SAVE:
        case ACTIONS.COURSE_UPDATE:
            return {
                ...state,
                waitingForSavingCourse: true,
            };
        case ACTIONS.COURSE_GET_TEACHER_COURSES:
        case ACTIONS.COURSE_GET_TEACHER_COURSE_BY_TEACHER_ID:
            return {
                ...state,
                isLoading: true,
            };
        case ACTIONS.COURSE_SAVE_FAIL:
        case ACTIONS.COURSE_UPDATE_FAIL:
        case ACTIONS.COURSE_GET_TEACHER_COURSES_FAIL:
        case ACTIONS.COURSE_GET_TEACHER_COURSE_BY_TEACHER_ID_FAIL:
            if (action.error.response !== undefined) {
                return {
                    ...state,
                    teacherCourses: [],
                    isLoading: false,
                    hasError: true,
                    errorMessage: action.error.response.data.message,
                    error: action.error.response.data.error
                };
            } else {
                return {
                    ...state,
                    teacherCourses: [],
                    isLoading: false,
                    hasError: true,
                    errorMessage: action.error.data
                }
            }
        case ACTIONS.COURSE_SAVE_RESOLVED_ERROR:
        case ACTIONS.COURSE_UPDATE_RESOLVED_ERROR:
        case ACTIONS.COURSE_GET_TEACHER_COURSES_RESOLVED_ERROR:
        case ACTIONS.COURSE_GET_TEACHER_COURSE_BY_TEACHER_ID_RESOLVED_ERROR:
            return {
                ...state,
                isLoading: false,
                hasError: false,
                errorMessage: '',
                error: '',
            };
        case ACTIONS.COURSE_SAVE_SUCCESS:
        case ACTIONS.COURSE_UPDATE_SUCCESS:
            return {
                ...state,
                statusCode: action.payload.status,
                hasError: false,
                waitingForSavingCourse: false
            };
        case ACTIONS.COURSE_GET_TEACHER_COURSES_SUCCESS:
        case ACTIONS.COURSE_GET_TEACHER_COURSE_BY_TEACHER_ID_SUCCESS:
            return {
                ...state,
                teacherCourses: action.payload.data,
                hasError: false,
                isLoading: false
            };
        default:
            return state;
    }
}