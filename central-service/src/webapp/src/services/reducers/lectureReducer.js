import { ACTIONS } from '../actions/lectureActions';
const initialState = {
    lecturesByTeacherId: [],
    courses: [],
    isLoading: false,
    hasError: false,
    errorMessage: '',
    error: '',
}

export default function lectureReducer(state = initialState, action) {

    switch (action.type) {
        case ACTIONS.GET_COURSES_BY_LECTURE:
        case ACTIONS.GET_LECTURES_BY_TEACHER_ID:
            return {
                ...state,
                isLoading: true
            };
        case ACTIONS.GET_COURSES_BY_LECTURE_FAIL:
        case ACTIONS.GET_LECTURES_BY_TEACHER_ID_FAIL:
            if (action.error.response !== undefined) {
                return {
                    ...state,
                    courses: [],
                    isLoading: false,
                    hasError: true,
                    errorMessage: action.error.response.data.message,
                    error: action.error.response.data.error
                };
            } else {
                return {
                    ...state,
                    courses: [],
                    isLoading: false,
                    hasError: true,
                    errorMessage: action.error.data
                };
            }
        case ACTIONS.GET_COURSES_BY_LECTURE_ERROR:
        case ACTIONS.GET_LECTURES_BY_TEACHER_ID_RESOLVED_ERROR:
            return {
                ...state,
                isLoading: false,
                hasError: false,
                errorMessage: '',
                error: '',
            };
        case ACTIONS.GET_COURSES_BY_LECTURE_SUCCESS:
            return {
                ...state,
                isLoading: false,
                courses: action.payload.data,
            }
        case ACTIONS.GET_LECTURES_BY_TEACHER_ID_SUCCESS:
            return {
                ...state,
                isLoading: false,
                lecturesByTeacherId: action.payload.data,
            }
        default:
            return state;
    }
}