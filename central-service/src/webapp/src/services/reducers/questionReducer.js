import { ACTIONS } from "../actions/questionActions";

const initialState = {
    unansweredQuestions: [],
    answeredQuestions: [],
    isLoading: false,
    waitingForPersistingTheAnswer: false,
    hasError: false,
    errorMessage: '',
    error: '',
    statusCode: 0
};


export default function questionReducer(state = initialState, action) {


    switch (action.type) {

        case ACTIONS.TEACHER_ANSWER_QUESTION:
            return {
                ...state,
                waitingForPersistingTheAnswer: true,
            }
        case ACTIONS.TEACHER_GET_ALL_UNANSWERED_QUESTIONS:
        case ACTIONS.TEACHER_GET_ALL_ANSWERED_QUESTIONS:
            return {
                ...state,
                isLoading: true,
            };
        case ACTIONS.TEACHER_ANSWER_QUESTION_FAIL:
        case ACTIONS.TEACHER_GET_ALL_UNANSWERED_QUESTIONS_FAIL:
        case ACTIONS.TEACHER_GET_ALL_ANSWERED_QUESTIONS_FAIL:
            if (action.error.response !== undefined) {
                return {
                    ...state,
                    unansweredQuestions: [],
                    isLoading: false,
                    waitingForPersistingTheAnswer: false,
                    hasError: true,
                    errorMessage: action.error.response.data.message,
                    error: action.error.response.data.error
                };
            } else {
                return {
                    ...state,
                    unansweredQuestions: [],
                    isLoading: false,
                    waitingForPersistingTheAnswer: false,
                    hasError: true,
                    errorMessage: action.error.data
                }
            }
        case ACTIONS.TEACHER_ANSWER_QUESTION_RESOLVED_ERROR:
        case ACTIONS.TEACHER_GET_ALL_UNANSWERED_QUESTIONS_RESOLVED_ERROR:
        case ACTIONS.TEACHER_GET_ALL_ANSWERED_QUESTIONS_RESOLVED_ERROR:
            return {
                ...state,
                isLoading: false,
                waitingForPersistingTheAnswer: false,
                hasError: false,
                errorMessage: '',
                error: '',
            };
        case ACTIONS.TEACHER_ANSWER_QUESTION_SUCCESS:
            return {
                ...state,
                waitingForPersistingTheAnswer: false,
                hasError: false,
                errorMessage: '',
                error: '',
                statusCode: action.payload.status,
            };
        case ACTIONS.TEACHER_GET_ALL_UNANSWERED_QUESTIONS_SUCCESS:
            return {
                ...state,
                unansweredQuestions: action.payload.data,
                hasError: false,
                isLoading: false
            };
        case ACTIONS.TEACHER_GET_ALL_ANSWERED_QUESTIONS_SUCCESS:
            return {
                ...state,
                answeredQuestions: action.payload.data,
                hasError: false,
                isLoading: false
            };
        default:
            return state;
    }
}