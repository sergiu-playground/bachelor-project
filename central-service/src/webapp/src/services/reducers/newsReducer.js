import {ACTIONS} from "../actions/newsActions"

const initialState = {
    newsFeed: [],
    isLoading: false,
    hasErrored: false,
    errorMessage: '',
    error: ''
}


export default function newsReducer (state = initialState, action) {
    
    switch (action.type) {
        case ACTIONS.GET_NEWS_FEED:
        return {
            ...state,
            isLoading: true,
        };
        case ACTIONS.GET_NEWS_FEED_FAIL:
        if (action.error.response !== undefined) {
            return {
                ...state,
                newsFeed: [],
                isLoading: false,
                hasError: true,
                errorMessage: action.error.response.data.message,
                error: action.error.response.data.error
            };
        } else {
            return {
                ...state,
                newsFeed: [],
                isLoading: false,
                hasError: true,
                errorMessage: action.error.data
            }
        }
        case ACTIONS.GET_NEWS_FEED_RESOLVED_ERROR:
        return{
            ...state,
            isLoading: false,
            hasErrored: false,
            errorMessage: '',
            error: ''
        };
        case ACTIONS.GET_NEWS_FEED_SUCCESS:
        return{
            ...state,
            newsFeed: action.payload.data,
            hasErrored:false,
            isLoading: false
        };
        default:
            return state;
    }
}