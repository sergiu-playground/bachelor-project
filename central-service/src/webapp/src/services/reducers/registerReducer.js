import { ACTIONS } from "../actions/registerActions";

const initialState = {
    data: [],
    isLoading: false,
    hasErrored: false,
    errorMessage: '',
    error: '',
    statusCode: 0
};

export default function registerReducer(state = initialState, action) {

    switch (action.type) {
        case ACTIONS.REGISTER_USER:
            return {
                ...state,
                isLoading: true,
            };
        case ACTIONS.REGISTER_USER_SUCCESS:
            return {
                ...state,
                statusCode: action.payload.status,
                hasErrored:false,
                isLoading:false
            };
        case ACTIONS.REGISTER_USER_FAIL:
            return {
                ...state,
                isLoading: false,
                hasErrored: true,
                errorMessage: action.error.response.data.message,
                error: action.error.data
            };
        case ACTIONS.RESOLVED_ERROR:
            return {
                ...state,
                isLoading: false,
                hasErrored: false,
                errorMessage: '',
                error: '',
                statusCode: 0
            };
        default:
            return state;
    }
}