import { ACTIONS } from "../actions/teacherActions";

const initialState = {
    allTeachers: [],
    feed: [],
    teacher: null,
    isLoading: false,
    waitingForUpdateTeacherData: false,
    hasError: false,
    errorMessage: '',
    error: '',
    statusCode: 0
};

export default function teacherReducer(state = initialState, action) {

    switch (action.type) {
        case ACTIONS.GET_ALL:
        case ACTIONS.ADD_STATUS:
        case ACTIONS.GET_FEED_FOR_TEACHER:
        case ACTIONS.GET_TEACHER_BY_ID:
            return {
                ...state,
                isLoading: true,
            };
            
        case ACTIONS.UPDATE_TEACHER:
        return {
            ...state,
            waitingForUpdateTeacherData: true
        }
        case ACTIONS.GET_ALL_FAIL:
        case ACTIONS.ADD_STATUS_FAIL:
        case ACTIONS.GET_FEED_FOR_TEACHER_FAIL:
        case ACTIONS.UPDATE_TEACHER_FAIL:
        case ACTIONS.GET_TEACHER_BY_ID_FAIL:
            if (action.error.response !== undefined) {
                return {
                    ...state,
                    allTeachers: [],
                    isLoading: false,
                    hasError: true,
                    errorMessage: action.error.response.data.message,
                    error: action.error.response.data.error
                };
            } else {
                return {
                    ...state,
                    allTeachers: [],
                    isLoading: false,
                    hasError: true,
                    errorMessage: action.error.data
                }
            }
        case ACTIONS.GET_ALL_RESOLVED_ERROR:
        case ACTIONS.ADD_STATUS_RESOLVED_ERROR:
        case ACTIONS.GET_FEED_FOR_TEACHER_RESOLVED_ERROR:
        case ACTIONS.UPDATE_TEACHER_RESOLVED_ERROR:
        case ACTIONS.GET_TEACHER_BY_ID_RESOLVED_ERROR:
            return {
                ...state,
                isLoading: false,
                hasError: false,
                errorMessage: '',
                error: '',
            };
        case ACTIONS.GET_ALL_SUCCESS:
            return {
                ...state,
                allTeachers: action.payload.data,
                hasError: false,
                isLoading: false
            };
        case ACTIONS.ADD_STATUS_SUCCESS:
            return {
                ...state,
                statusCode: action.payload.status,
                hasError: false,
                isLoading: false
            };
        case ACTIONS.UPDATE_TEACHER_SUCCESS:
        return {
            ...state,
            statusCode: action.payload.status,
            hasError: false,
            waitingForUpdateTeacherData: false
        };
        case ACTIONS.GET_FEED_FOR_TEACHER_SUCCESS:
            return {
                ...state,
                feed: action.payload.data,
                hasError: false,
                isLoading: false
            };
        case ACTIONS.GET_TEACHER_BY_ID_SUCCESS:
            return {
                ...state,
                teacher: action.payload.data,
                hasError: false,
                isLoading: false
            };
        default:
            return state;
    }
}