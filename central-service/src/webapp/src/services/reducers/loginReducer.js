import { ACTIONS } from "../actions/loginActions";

const initialState = {
    isLoading: false,
    isAuthenticated: false,
    hasError: false,
    errorMessage: '',
    error: '',
    userRole: '',
    userId: '',
    statusCode: 0
};

export default function loginReducer(state = initialState, action) {

    switch (action.type) {
        case ACTIONS.LOGIN_USER:
        case ACTIONS.LOGOUT_USER:
            return {
                ...state,
                isLoading: true
            };
        case ACTIONS.LOGIN_USER_SUCCESS:
            return {
                ...state,
                statusCode: action.payload.status,
                userRole: action.payload.data.roles[0].code,
                userId: action.payload.data.id,
                hasError: false,
                isLoading: false,
                isAuthenticated: true
            };
        case ACTIONS.LOGIN_USER_FAIL :
        case ACTIONS.LOGOUT_USER_FAIL :
        if(action.error.response !== undefined){
            return {
                ...state,
                isLoading: false,
                hasError: true,
                errorMessage: action.error.response.data.message,
                error: action.error.response.data.error,
                isAuthenticated: false
            };
        } else {
            return {
                ...state,
                isLoading: false,
                hasError: true,
                errorMessage: action.error.data,
                isAuthenticated: false
            }
        }
        case ACTIONS.RESOLVED_ERROR:
            return {
                ...state,
                isLoading: false,
                hasError: false,
                errorMessage: '',
                error: '',
                statusCode: 0
            };
        case ACTIONS.LOGOUT_USER_SUCCESS:
            return {
                ...state,
                statusCode: action.payload.status,
                hasError: false,
                isLoading: false,
                isAuthenticated: false
            };
        default:
            return state;
    }
}