import { combineReducers } from 'redux';
import register from './registerReducer';
import news from './newsReducer';
import teachers from './teacherReducer';
import login from './loginReducer';
import course from './courseReducer';
import questions from './questionReducer';
import lectures from './lectureReducer';

const rootReducer = combineReducers({
    register,
    news,
    teachers,
    login,
    course,
    questions,
    lectures
});

export default rootReducer;