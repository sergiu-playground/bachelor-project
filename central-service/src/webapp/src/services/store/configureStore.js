import rootReducer from "../reducers";
import { applyMiddleware, createStore, compose } from "redux";
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk'
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import { httpApiUrl } from "../constants";
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
    key: 'root',
    storage,
}
const persistedReducer = persistReducer(persistConfig, rootReducer)

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const client = axios.create(
    {
        baseURL: httpApiUrl,
        responseType: 'json',
        withCredentials: true,
        headers: {
            "Access-Control-Allow-Origin": "*"
        }
    }
);

export default function configureStore(initialState) {

    let store = createStore(
        persistedReducer,
        initialState,
        composeEnhancers(
            applyMiddleware(thunk,
                axiosMiddleware(client),
                reduxImmutableStateInvariant()
            )
        )
    )
    let persistor = persistStore(store)
    return { store, persistor };
}