export const ACTIONS = {
    GET_NEWS_FEED: 'GET_NEWS_FEED',
    GET_NEWS_FEED_SUCCESS: 'GET_NEWS_FEED_SUCCESS',
    GET_NEWS_FEED_FAIL: 'GET_NEWS_FEED_FAIL',
    GET_NEWS_FEED_RESOLVED_ERROR: 'GET_NEWS_FEED_RESOLVED_ERROR',
}

export function getNewsFeed() {
    return {
        type: ACTIONS.GET_NEWS_FEED,
        payload: {
            request: {
                method: 'GET',
                url: '/newsFeed/news'
            }
        }
    }
}