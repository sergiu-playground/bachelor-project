export const ACTIONS = {
    GET_ALL: 'GET_ALL',
    GET_ALL_SUCCESS: 'GET_ALL_SUCCESS',
    GET_ALL_FAIL: 'GET_ALL_FAIL',
    GET_ALL_RESOLVED_ERROR: 'GET_ALL_RESOLVED_ERROR',

    ADD_STATUS: 'ADD_STATUS',
    ADD_STATUS_SUCCESS: 'ADD_STATUS_SUCCESS',
    ADD_STATUS_FAIL: 'ADD_STATUS_FAIL',
    ADD_STATUS_RESOLVED_ERROR: 'ADD_STATUS_RESOLVED_ERROR',

    GET_FEED_FOR_TEACHER: 'GET_FEED_FOR_TEACHER',
    GET_FEED_FOR_TEACHER_SUCCESS: 'GET_FEED_FOR_TEACHER_SUCCESS',
    GET_FEED_FOR_TEACHER_FAIL: 'GET_FEED_FOR_TEACHER_FAIL',
    GET_FEED_FOR_TEACHER_RESOLVED_ERROR: 'GET_FEED_FOR_TEACHER_RESOLVED_ERROR',

    UPDATE_TEACHER: 'UPDATE_TEACHER',
    UPDATE_TEACHER_SUCCESS: 'UPDATE_TEACHER_SUCCESS',
    UPDATE_TEACHER_FAIL: 'UPDATE_TEACHER_FAIL',
    UPDATE_TEACHER_RESOLVED_ERROR: 'UPDATE_TEACHER_RESOLVED_ERROR',
    
    GET_TEACHER_BY_ID: 'GET_TEACHER_BY_ID',
    GET_TEACHER_BY_ID_SUCCESS: 'GET_TEACHER_BY_ID_SUCCESS',
    GET_TEACHER_BY_ID_FAIL: 'GET_TEACHER_BY_ID_FAIL',
    GET_TEACHER_BY_ID_RESOLVED_ERROR: 'GET_TEACHER_BY_ID_RESOLVED_ERROR',
}

export function getAllTeachers() {
    return {
        type: ACTIONS.GET_ALL,
        payload: {
            request: {
                method: 'GET',
                url: '/teacher/getAll'
            }
        }
    }
}

export function addStatus(statusData) {
    return {
        type: ACTIONS.ADD_STATUS,
        payload: {
            request: {
                method: 'POST',
                url: '/status/saveStatus',
                data: {
                    ...statusData
                }
            }
        }
    }
}

export function getFeedForTeacher(){
    return {
        type: ACTIONS.GET_FEED_FOR_TEACHER,
        payload: {
            request: {
                method: 'GET',
                url: '/feed/getFeedForTeacher'
            }
        }
    }
}

export function update(teacher){
    return {
        type: ACTIONS.UPDATE_TEACHER,
        payload: {
            request: {
                method: 'PUT',
                url: '/teacher/',
                data: teacher
            }
        }
    }
}

export function getTeacherById(id){
    return {
        type: ACTIONS.GET_TEACHER_BY_ID,
        payload: {
            request: {
                method: 'GET',
                url: '/teacher/findById/' + id
            }
        }
    }
}

export function resolvedErrorGetAll() {

    return {
        type:ACTIONS.GET_ALL_RESOLVED_ERROR,
    }
}