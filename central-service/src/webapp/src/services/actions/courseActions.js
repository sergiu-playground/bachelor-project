export const ACTIONS = {
    COURSE_SAVE: 'COURSE_SAVE',
    COURSE_SAVE_SUCCESS: 'COURSE_SAVE_SUCCESS',
    COURSE_SAVE_FAIL: 'COURSE_SAVE_FAIL',
    COURSE_SAVE_RESOLVED_ERROR: 'COURSE_SAVE_RESOLVED_ERROR',

    COURSE_UPDATE: 'COURSE_UPDATE',
    COURSE_UPDATE_SUCCESS: 'COURSE_UPDATE_SUCCESS',
    COURSE_UPDATE_FAIL: 'COURSE_UPDATE_FAIL',
    COURSE_UPDATE_RESOLVED_ERROR: 'COURSE_UPDATE_RESOLVED_ERROR',

    COURSE_GET_TEACHER_COURSES: 'COURSE_GET_TEACHER_COURSES',
    COURSE_GET_TEACHER_COURSES_SUCCESS: 'COURSE_GET_TEACHER_COURSES_SUCCESS',
    COURSE_GET_TEACHER_COURSES_FAIL: 'COURSE_GET_TEACHER_COURSES_FAIL',
    COURSE_GET_TEACHER_COURSES_RESOLVED_ERROR: 'COURSE_GET_TEACHER_COURSES_RESOLVED_ERROR',

    COURSE_GET_TEACHER_COURSE_BY_TEACHER_ID: 'COURSE_GET_TEACHER_COURSE_BY_TEACHER_ID',
    COURSE_GET_TEACHER_COURSE_BY_TEACHER_ID_SUCCESS: 'COURSE_GET_TEACHER_COURSE_BY_TEACHER_ID_SUCCESS',
    COURSE_GET_TEACHER_COURSE_BY_TEACHER_ID_FAIL: 'COURSE_GET_TEACHER_COURSE_BY_TEACHER_ID_FAIL',
    COURSE_GET_TEACHER_COURSE_BY_TEACHER_ID_RESOLVED_ERROR: 'COURSE_GET_TEACHER_COURSE_BY_TEACHER_ID_RESOLVED_ERROR',
}

export function saveCourse(courseData) {
    return {
        type: ACTIONS.COURSE_SAVE,
        payload: {
            request: {
                method: 'POST',
                url: '/course/',
                data: courseData,
            }
        }
    }
}

export function update(courseData, courseId) {
    return {
        type: ACTIONS.COURSE_UPDATE,
        payload: {
            request: {
                method: 'PUT',
                url: '/course/' + courseId,
                data: courseData,
            }
        }
    }
}

export function getTeacherCourses(){
    return {
        type: ACTIONS.COURSE_GET_TEACHER_COURSES,
        payload: {
            request: {
                method: 'GET',
                url: '/course/getTeacherCourses',
            }
        }
    }
}

export function getTeacherCoursesByTeacherId(teacherId){
    return {
        type: ACTIONS.COURSE_GET_TEACHER_COURSE_BY_TEACHER_ID,
        payload: {
            request: {
                method: 'GET',
                url: '/course/getTeacherCourses/' + teacherId,
            }
        }
    }
}