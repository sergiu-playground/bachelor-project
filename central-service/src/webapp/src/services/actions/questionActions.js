export const ACTIONS = {
    TEACHER_ANSWER_QUESTION: 'TEACHER_ANSWER_QUESTION',
    TEACHER_ANSWER_QUESTION_SUCCESS: 'TEACHER_ANSWER_QUESTION_SUCCESS',
    TEACHER_ANSWER_QUESTION_FAIL: 'TEACHER_ANSWER_QUESTION_FAIL',
    TEACHER_ANSWER_QUESTION_RESOLVED_ERROR: 'TEACHER_ANSWER_QUESTION_RESOLVED_ERROR',

    
    TEACHER_GET_ALL_UNANSWERED_QUESTIONS: 'TEACHER_GET_ALL_UNANSWERED_QUESTIONS',
    TEACHER_GET_ALL_UNANSWERED_QUESTIONS_SUCCESS: 'TEACHER_GET_ALL_UNANSWERED_QUESTIONS_SUCCESS',
    TEACHER_GET_ALL_UNANSWERED_QUESTIONS_FAIL: 'TEACHER_GET_ALL_UNANSWERED_QUESTIONS_FAIL',
    TEACHER_GET_ALL_UNANSWERED_QUESTIONS_RESOLVED_ERROR: 'TEACHER_GET_ALL_UNANSWERED_QUESTIONS_RESOLVED_ERROR',
    

    TEACHER_GET_ALL_ANSWERED_QUESTIONS: 'TEACHER_GET_ALL_ANSWERED_QUESTIONS',
    TEACHER_GET_ALL_ANSWERED_QUESTIONS_SUCCESS: 'TEACHER_GET_ALL_ANSWERED_QUESTIONS_SUCCESS',
    TEACHER_GET_ALL_ANSWERED_QUESTIONS_FAIL: 'TEACHER_GET_ALL_ANSWERED_QUESTIONS_FAIL',
    TEACHER_GET_ALL_ANSWERED_QUESTIONS_RESOLVED_ERROR: 'TEACHER_GET_ALL_ANSWERED_QUESTIONS_RESOLVED_ERROR',
}

export function answerQuestion(answer) {
    return {
        type: ACTIONS.TEACHER_ANSWER_QUESTION,
        payload: {
            request: {
                method: 'POST',
                url: '/question/answerQuestion',
                data: {
                    ...answer
                }
            }
        }
    }
}

export function getAllUnansweredQuestions(){
    return {
        type: ACTIONS.TEACHER_GET_ALL_UNANSWERED_QUESTIONS,
        payload: {
            request: {
                method: 'GET',
                url: '/question/unansweredQuestions'
            }
        }
    }
}

export function getTeacherAnsweredQuestionsByTeacherId(teacherId){
    return {
        type: ACTIONS.TEACHER_GET_ALL_ANSWERED_QUESTIONS,
        payload: {
            request: {
                method: 'GET',
                url: '/question/teacherAnsweredQuestions/' + teacherId
            }
        }
    }
}