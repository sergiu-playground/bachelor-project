export const ACTIONS = {
    SAVE_QUESTION: 'SAVE_QUESTION',
    SAVE_QUESTION_SUCCESS: 'SAVE_QUESTION_SUCCESS',
    SAVE_QUESTION_FAIL: 'SAVE_QUESTION_FAIL',
    SAVE_QUESTION_RESOLVED_ERROR: 'SAVE_QUESTION_RESOLVED_ERROR',
}

export function askQuestion(questionData) {
    return {
        type: ACTIONS.SAVE_QUESTION,
        payload: {
            request: {
                method: 'POST',
                url: '/question/saveQuestion',
                data: {
                    ...questionData
                }
            }
        }
    }
}