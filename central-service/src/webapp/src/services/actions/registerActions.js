export const ACTIONS = {
    REGISTER_USER: 'REGISTER_USER',
    REGISTER_USER_SUCCESS: 'REGISTER_USER_SUCCESS',
    REGISTER_USER_FAIL: 'REGISTER_USER_FAIL',
    RESOLVED_ERROR: 'RESOLVED_ERROR',
};

export function registerNewUser(userData) {
    return {
        type: ACTIONS.REGISTER_USER,
        payload: {
            request: {
                method: 'POST',
                url: '/registration/',
                data: {
                    ...userData
                }
            }
        }
    }
}

export function resolvedError() {

    return {
        type:ACTIONS.RESOLVED_ERROR,
    }
}