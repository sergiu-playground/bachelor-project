export const ACTIONS = {
    LOGIN_USER: 'LOGIN_USER',
    LOGIN_USER_SUCCESS: 'LOGIN_USER_SUCCESS',
    LOGIN_USER_FAIL: 'LOGIN_USER_FAIL',
    LOGOUT_USER: 'LOGOUT_USER',
    LOGOUT_USER_SUCCESS: 'LOGOUT_USER_SUCCESS',
    LOGOUT_USER_FAIL: 'LOGOUT_USER_FAIL',
    RESOLVED_ERROR: 'RESOLVED_ERROR',
};

export function loginUser(userLoginData) {
    return {
        type: ACTIONS.LOGIN_USER,
        payload: {
            request: {
                method: 'POST',
                url: '/login',
                data: {
                    ...userLoginData
                }
            }
        }
    }
}

export function logOutUser() {
    return {
        type: ACTIONS.LOGOUT_USER,
        payload: {
            request: {
                method: 'GET',
                url: '/logout',
            }
        }
    }
}

export function resolvedError() {

    return {
        type:ACTIONS.RESOLVED_ERROR,
    }
}