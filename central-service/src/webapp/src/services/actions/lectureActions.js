export const ACTIONS = {

    GET_COURSES_BY_LECTURE: 'GET_COURSES_BY_LECTURE',
    GET_COURSES_BY_LECTURE_SUCCESS: 'GET_COURSES_BY_LECTURE_SUCCESS',
    GET_COURSES_BY_LECTURE_FAIL: 'GET_COURSES_BY_LECTURE_FAIL',
    GET_COURSES_BY_LECTURE_RESOLVED_ERROR: 'GET_COURSES_BY_LECTURE_RESOLVED_ERROR',

    GET_LECTURES_BY_TEACHER_ID: 'GET_LECTURES_BY_TEACHER_ID',
    GET_LECTURES_BY_TEACHER_ID_SUCCESS: 'GET_LECTURES_BY_TEACHER_ID_SUCCESS',
    GET_LECTURES_BY_TEACHER_ID_FAIL: 'GET_LECTURES_BY_TEACHER_ID_FAIL',
    GET_LECTURES_BY_TEACHER_ID_RESOLVED_ERROR: 'GET_LECTURES_BY_TEACHER_ID_RESOLVED_ERROR'
}

export function getCoursesByLecture(lecture) {
    return {
        type: ACTIONS.GET_COURSES_BY_LECTURE,
        payload: {
            request: {
                method: 'GET',
                url: '/lecture/' + lecture,
            }
        }
    }

}

export function getLecturesByTeacherId(teacherId) {
    return {
        type: ACTIONS.GET_LECTURES_BY_TEACHER_ID,
        payload: {
            request: {
                method: 'GET',
                url: '/lecture/getLecturesByTeacherId/' + teacherId,
            }
        }
    }
}