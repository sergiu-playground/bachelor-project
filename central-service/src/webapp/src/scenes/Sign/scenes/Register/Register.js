import React, { Component } from 'react';
import RegisterForm from "./RegisterForm";
import './Register.scss';
import { resolvedError } from "../../../../services/actions/registerActions";
import { connect } from "react-redux";
import ErrorMessage from "../../../../components/ErrorMessage";
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Row, Col, Container } from 'react-bootstrap'

class Register extends Component {

    render() {
        return (
            <Container fluid={true} >
                <Row className="justify-content-md-center">
                    <Col xs={{ order: 1 }} md="4">
                        <RegisterForm />
                        <div className="registerForm-signIn">
                            <p>
                                <Link to='/'>Sign in</Link>
                            </p>
                        </div>
                    </Col>
                    {this.props.hasErrored ?
                        <ErrorMessage text={this.props.errorMessage} callback={() => this.props.onClickErrorOk()} /> :
                        null}
                    {
                        this.props.statusCode === 200 ?
                            <ErrorMessage text="Successfully registered!" callback={() => this.props.onClickRegisterOk()} />
                            : null
                    }
                </Row>
            </Container>
        );
    }

}

Register.propTypes = {
    hasErrored: PropTypes.bool.isRequired,
};


function mapStateToProps(state) {
    return {
        hasErrored: state.register.hasErrored,
        errorMessage: state.register.errorMessage,
        statusCode: state.register.statusCode
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        onClickErrorOk: () => { dispatch(resolvedError()) },
        onClickRegisterOk: () => {
            dispatch(resolvedError());
            return <Link to='/'></Link>;
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);