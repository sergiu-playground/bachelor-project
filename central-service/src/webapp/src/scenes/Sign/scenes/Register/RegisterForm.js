import React, { Component } from 'react';
import {
    Button,
    Col,
    Form,
    FormControl,
    Row
} from "react-bootstrap";
import { connect } from 'react-redux';
import './RegisterForm.scss';
import { registerNewUser } from "../../../../services/actions/registerActions";


class RegisterForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            confirmPassword: '',
            firstName: '',
            lastName: '',
            birthday: '',
            year: '',
            semester: '',
            group: '',
            registrationType: ''

        };

        this.handleOnChange = this.handleOnChange.bind(this);
    }

    handleOnChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    render() {
        return (
                <Form className="form-container register-form">
                    <Form.Group>
                        <Form.Control
                            type="email"
                            placeholder="Email"
                            name="email"
                            value={this.state.email}
                            onChange={this.handleOnChange}
                        />
                    </Form.Group>

                    <Form.Row>
                        <Col>
                            <Form.Group>
                                <Form.Control
                                    type="password"
                                    name="password"
                                    placeholder="Password"
                                    value={this.state.password}
                                    onChange={this.handleOnChange}
                                />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Control
                                    type="password"
                                    name="confirmPassword"
                                    placeholder="Confirm Password"
                                    value={this.state.confirmPassword}
                                    onChange={this.handleOnChange}
                                />
                            </Form.Group>
                        </Col>
                    </Form.Row>
                    <Form.Row>
                        <Col>
                            <Form.Group>
                                <Form.Control
                                    type="text"
                                    name="firstName"
                                    placeholder="First name"
                                    value={this.state.firstName}
                                    onChange={this.handleOnChange}
                                />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Control
                                    type="text"
                                    name="lastName"
                                    placeholder="Last name"
                                    value={this.state.lastName}
                                    onChange={this.handleOnChange}
                                />
                            </Form.Group>
                        </Col>
                    </Form.Row>
                    <Form.Group>
                        <Form.Label style={{ color: "rgb(19, 169, 214)" }}>
                            Birthday
                            </Form.Label>
                        <FormControl
                            type="date"
                            name="birthday"
                            placeholder="Birthday"
                            value={this.state.birthday}
                            onChange={this.handleOnChange}
                        />
                    </Form.Group>
                    <Form.Row>
                        <Col>
                            <Form.Group>
                                <Form.Control
                                    type="number"
                                    name="year"
                                    placeholder="Year"
                                    value={this.state.year}
                                    onChange={this.handleOnChange}
                                />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Control
                                    type="number"
                                    name="semester"
                                    placeholder="Semester"
                                    value={this.state.semester}
                                    onChange={this.handleOnChange}
                                />
                            </Form.Group>
                        </Col>
                    </Form.Row>
                    <Form.Group>
                        <Form.Control
                            type="number"
                            name="group"
                            placeholder="Group"
                            value={this.state.group}
                            onChange={this.handleOnChange}
                        />
                    </Form.Group>
                    <Form >
                        <Row inline>
                            <Col xs={{ order: 1 }} sm={4}>
                                <Form.Check
                                    type="radio"
                                    id={`check-api-radio-2`}>
                                    <Form.Check.Input
                                        type="radio"
                                        isValid
                                        onChange={this.handleOnChange}
                                        value="Student"
                                        name="registrationType" />{'  '}
                                    <Form.Check.Label><a className="radioButtons">Student</a></Form.Check.Label>
                                </Form.Check>
                            </Col>
                            <Col xs={{ order: 2 }} sm={4}>
                                <Form.Check
                                    type="radio"
                                    id={`check-api-radio-3`}>
                                    <Form.Check.Input
                                        type="radio"
                                        isValid
                                        onChange={this.handleOnChange}
                                        value="Teacher"
                                        name="registrationType" />{'  '}
                                    <Form.Check.Label><a className="radioButtons">Teacher</a></Form.Check.Label>
                                </Form.Check>
                            </Col>
                        </Row>
                    </Form>
                    <br></br>
                    <Button
                        type="button"
                        className="btn-block registerForm-group btn-for-reg"
                        onClick={() => this.props.onTodoClick(this.state)}
                    >
                        Register
                            </Button>
                </Form>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        onTodoClick: (userData) => {
            dispatch(registerNewUser(userData))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterForm);