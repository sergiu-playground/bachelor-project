import React, { Component } from 'react';
import LoginForm from "./LoginForm";
// import "./Login.scss";
import ErrorMessage from "../../../../components/ErrorMessage";
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import { resolvedError } from "../../../../services/actions/loginActions";
import { Redirect, Link } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';

class Login extends Component {

    render() {
        return (

                <Container fluid={true} >
                    <Row className="justify-content-md-center"> 
                        <Col xs={{ order: 1 }} md="4">
                            <LoginForm />

                            <div>
                                <div className="createAccount">
                                    <p>
                                        <Link to='/registration'>Create account</Link>
                                    </p>
                                </div>
                                {/* <div className="forgotPassword">
                                    <p>
                                        <Link to='/newPage'>Forgot password</Link>
                                    </p>
                                </div> */}
                            </div>
                        </Col>
                        {this.props.hasError ?
                            <ErrorMessage text={this.props.errorMessage} callback={() => this.props.onClickErrorOk()} /> :
                            null}
                        {
                            this.props.isAuthenticated ?
                                <Redirect to={{ pathname: '/newsFeed' }} />
                                : null
                        }
                    </Row>
                </Container>

        );
    }
}

Login.propTypes = {
    hasError: PropTypes.bool.isRequired,
};


function mapStateToProps(state) {
    return {
        hasError: state.login.hasError,
        errorMessage: state.login.errorMessage,
        isAuthenticated: state.login.isAuthenticated
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        onClickErrorOk: () => { dispatch(resolvedError()) },
        onClickRegisterOk: () => {
            dispatch(resolvedError());
            return <Link to='/'></Link>;
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);