import React, { Component } from 'react';
import {
    Button,
    Form
} from "react-bootstrap";
import { Link } from 'react-router-dom';
import "./LoginForm.scss";
import { connect } from 'react-redux';
import { loginUser } from "../../../../services/actions/loginActions";

class LoginForm extends Component {
    constructor(props) {
        super(props);

        this.handleOnChange = this.handleOnChange.bind(this);
        this.onClickRegister = this.onClickRegister.bind(this);
        this.onClickForgotPassword = this.onClickForgotPassword.bind(this);

        this.state = {
            email: '',
            password: ''
        };
    }

    onClickRegister() {
        return <Link to='/registration'></Link>;
    }

    onClickForgotPassword() {
        alert(this.state.email)
    }

    handleOnChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    render() {
        return (
            <Form className="form-container login-form">
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Email"
                        name="email"
                        value={this.state.email}
                        onChange={this.handleOnChange}
                    />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Password"
                        name="password"
                        value={this.state.password}
                        onChange={this.handleOnChange}
                    />
                </Form.Group>
                {/* <Form.Group controlId="formBasicChecbox">
                    <Form.Check
                        type="checkbox"
                        label="Remember me" />
                </Form.Group> */}
                <Button
                    className="btn-block btn-for-reg"
                    onClick={() => this.props.onClickLogin(this.state)}
                >
                    Sign in
                    </Button>
            </Form>
        );
    }
}

function mapStateToProps(state) {
    return {
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        onClickLogin: (userData) => {
            dispatch(loginUser(userData))
        }
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);