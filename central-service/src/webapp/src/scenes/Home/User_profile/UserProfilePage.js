import React, { Component } from 'react';
import Menu from "../News_feed/Menu";
import ApplicationSign from '../News_feed/ApplicationSign';
import Container from 'react-bootstrap/Container';
import { Row, Col } from 'react-bootstrap';
import UserProfileSign from '../News_feed/UserProfileSign';
import LogOutButton from '../News_feed/LogOutButton';
import AddQuestion from './AddQuestion';
import AddStatus from './AddStatus';

class UserProfilePage extends Component {
    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {
        return (
            <Container fluid={true} >
                <Row>
                    <Col xs sm={2}>
                        <ApplicationSign />
                    </Col>
                    <Col xs={{ order: 1 }} sm={8}>
                        {/* <SearchBar /> */}
                    </Col>
                    <Col xs={{ order: 2 }} sm={2}>
                    <UserProfileSign/>
                    </Col>
                </Row>
                <Row>
                    <Col xs sm={2}>
                        <Menu />
                    </Col>
                    <Col xs={{ order: 1 }} sm={8}>
                        {/* TODO: Fix the errors when the connection is down. */}
                        {
                    this.props.role === "ROLE_STUDENT" ?
                        <AddQuestion />
                        : null
                }{
                    this.props.role === 'ROLE_TEACHER' ?
                        <AddStatus />
                        : null
                }
                    </Col>
                </Row>
                <Row>
                    <Col xs sm={2}>
                    <LogOutButton/>
                    </Col>
                </Row>
            </Container>
        );
    }
}

UserProfilePage.propTypes = {

};

export default UserProfilePage;