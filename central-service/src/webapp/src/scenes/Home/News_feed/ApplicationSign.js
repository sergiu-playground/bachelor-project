import React, { Component } from 'react';
import { connect } from "react-redux";
import { Navbar } from 'react-bootstrap';
import logo from '../../../media/pageImage.png';
import './ApplicationSign.scss'

class ApplicationSign extends Component {
    
    componentDidMount() {
    }

    componentWillUnmount() {
    }
    render() {
        return (
            <Navbar.Brand href="/newsFeed" >
                <img
                    src={logo}
                    width="40"
                    height="40"
                    className="d-inline-block align-top"
                    alt="React Bootstrap logo"
                />
            </Navbar.Brand>);
    }
}

ApplicationSign.propTypes = {

};

const mapStateToProps = (state) => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationSign);