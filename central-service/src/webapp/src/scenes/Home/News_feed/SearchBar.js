import React, { Component } from 'react';
import { connect } from "react-redux";
import {
    Button,
    Form,
    FormControl
} from "react-bootstrap";
class SearchBar extends Component {
    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {
        return (
            <Form inline>
            <FormControl type="text" placeholder="Search" className=" mr-sm-2" />
            <Button type="submit">Submit</Button>
          </Form>
        );
    }
}

SearchBar.propTypes = {

};

const mapStateToProps = (state) => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);