import React, { Component } from 'react';
import { connect } from "react-redux";
import { Button, InputGroup, FormControl } from 'react-bootstrap';
import { askQuestion } from '../../../services/actions/studentActions';
import "./AskQuestion.scss";

class AskQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            question: '',
            teacherName: '',
            questionerId: ''
        }

        this.handleOnChange = this.handleOnChange.bind(this);
        this.onPressClick = this.onPressClick.bind(this);
    }

    componentDidMount() {
        this.setState({ questionerId: this.props.questionerId });
    }
    componentWillUnmount() {

    }

    onPressClick() {

        this.props.onTodoClick(this.state)
        window.location.reload();
    }

    handleOnChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    render() {
        return (
            <div className="askQuestion-form">
                <InputGroup>
                    <InputGroup.Prepend>
                        <InputGroup.Text>Question</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                        as="textarea"
                        aria-label="With textarea"
                        name="question"
                        value={this.state.question}
                        onChange={this.handleOnChange}
                    />
                </InputGroup>
                <br></br>
                <InputGroup className="mb-3">
                    <FormControl
                        aria-label="Recipient's username"
                        aria-describedby="basic-addon2"
                        placeholder="Teacher's name"
                        name="teacherName"
                        value={this.state.teacherName}
                        onChange={this.handleOnChange}
                    />
                    <InputGroup.Append>
                        <Button
                            variant="outline-secondary"
                            onClick={this.onPressClick}
                        >
                            Ask
                        </Button>
                    </InputGroup.Append>
                </InputGroup>
            </div>
        );
    }
}

AskQuestion.propTypes = {

};

const mapStateToProps = (state) => {
    return {
        questionerId: state.login.userId
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onTodoClick: (data) => {
            dispatch(askQuestion(data))
        }

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AskQuestion);