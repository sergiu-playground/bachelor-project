import React, { Component } from 'react';
import { connect } from "react-redux";
import { InputGroup, FormControl, Button, Form } from 'react-bootstrap';
import { addStatus } from '../../../services/actions/teacherActions';
import "./AddStatus.scss";

class AddStatus extends Component {
    constructor(props) {
        super(props);

        this.state = {
            description: '',
            userId: ''
        }

        this.handleOnChange = this.handleOnChange.bind(this);
        this.onPressClick = this.onPressClick.bind(this);
    }

    componentDidMount() {
        this.setState({ userId: this.props.userId });
    }
    componentWillUnmount() {

    }

    onPressClick() {

        this.props.onTodoClick(this.state)
        this.setState({
            description: ''
        });
    }

    handleOnChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    render() {
        return (
            <div className="addStatus-form">
                <Form>
                    <Form.Label>Add Status</Form.Label>
                </Form>
                <InputGroup className="mb-3">
                    <FormControl
                        aria-label="Recipient's username"
                        aria-describedby="basic-addon2"
                        placeholder="Status"
                        name="description"
                        value={this.state.description}
                        onChange={this.handleOnChange}
                    />
                    <InputGroup.Append>
                        <Button
                            variant="outline-secondary"
                            type="button"
                            onClick={this.onPressClick}
                        >
                            Publish
                        </Button>
                    </InputGroup.Append>
                </InputGroup>
            </div>
        );
    }
}

AddStatus.propTypes = {

};

const mapStateToProps = (state) => {
    return {
        userId: state.login.userId,
        loadingStatus: state.teachers.isLoading,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onTodoClick: (data) => {
            dispatch(addStatus(data))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddStatus);