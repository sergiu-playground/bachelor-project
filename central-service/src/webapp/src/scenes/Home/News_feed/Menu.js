import React, { Component } from 'react';
import { connect } from "react-redux";
import {
    Nav,
} from "react-bootstrap";
import LogOutButton from './LogOutButton';


class Menu extends Component {

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {
        return (
                <Nav variant="pills" className="flex-column">
                    <Nav.Link href="/newsFeed">Home</Nav.Link>
                    <Nav.Link href="/teachers">Teachers</Nav.Link>
                    {/* <Nav.Link href="#" disabled>Lectures</Nav.Link>
                    <Nav.Link href="#" disabled>Schedule</Nav.Link> */}
                    <LogOutButton></LogOutButton>
                </Nav>
        );
    }
}

Menu.propTypes = {};

const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Menu);