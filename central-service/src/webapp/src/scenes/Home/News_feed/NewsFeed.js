import React, { Component } from 'react';
import { connect } from "react-redux";
import Menu from "./Menu";
import News from "./News/News";
import ApplicationSign from './ApplicationSign';
import Container from 'react-bootstrap/Container';
import { Row, Col, Navbar } from 'react-bootstrap';
import UserProfileSign from './UserProfileSign';
import AskQuestion from './AskQuestion';
import AddStatus from './AddStatus';

class NewsFeed extends Component {

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {
        // if (this.props.isLoadingStatus) {
        // return (<ProgressBar now={60} />)
        // } else {
        return (
            <Container fluid={true} >
                <Navbar sticky="top" fixed="top" bg="white" className="bg-light justify-content-between">
                    <ApplicationSign />
                    {/* <SearchBar /> */}
                    <UserProfileSign />
                </Navbar>
                <Row>
                    <Col xs sm={2}>
                        <Menu />
                    </Col>
                    <Col xs={{ order: 1 }} sm={8}>
                        {/* TODO: Fix the errors when the connection is down. */}
                        <Row>
                            {
                                this.props.userRole === "ROLE_TEACHER" ?
                                    <Col xs={{ order: 1 }}>
                                        <AddStatus />
                                    </Col>
                                    :
                                    <Col xs={{ order: 1 }}>
                                        <AskQuestion />
                                    </Col>
                            }
                        </Row>
                        <Row>
                            <News />
                        </Row>
                    </Col>
                </Row>
            </Container>
        );
    }
}
// }

NewsFeed.propTypes = {

};

const mapStateToProps = (state) => {
    return {
        userRole: state.login.userRole,
        isLoadingStatus: state.news.isLoading
    };
};

export default connect(mapStateToProps)(NewsFeed);