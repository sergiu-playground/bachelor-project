import React, { Component } from 'react';
import { connect } from "react-redux";
import { Button } from 'react-bootstrap';
import { logOutUser } from '../../../services/actions/loginActions';
import { Redirect } from 'react-router-dom';

class LogOutButton extends Component {
    state = {
        redirect: false
    };
    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
        this.renderRedirect = this.renderRedirect.bind(this);
    }

    componentDidMount() {
    }
    componentWillUnmount() {


    }

    handleClick() {
        this.props.onClickLogOut();
    }
    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to={{ pathname: '/' }} />
        }
    }
    render() {
        return (
            <div>
                {
                    this.props.isAuthenticated ?
                        null :
                        <Redirect to={{ pathname: '/' }} />
                }
                <Button
                    type="button"
                    className="btn-block btn-for-reg"
                    onClick={this.handleClick}
                >
                    Log out
            </Button>
            </div>
        );
    }
}

LogOutButton.propTypes = {

};

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.login.isAuthenticated
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onClickLogOut: () => {
            dispatch(logOutUser())
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LogOutButton);