import React, { Component } from 'react';
import { connect } from "react-redux";
import Question from './Question';
import Status from './Status';
import {
    getNewsFeed
} from "../../../../services/actions/newsActions"
import { ClipLoader } from 'react-spinners';
import { override } from '../../../../services/constants';

class News extends Component {

    componentDidMount() {
        // TODO: get user id from state
        this.props.dispatch(getNewsFeed());
    }

    componentDidUpdate(prevProps) {
        if (prevProps.loadingStatus !== this.props.loadingStatus &&
            prevProps.loadingStatus === true) {

            this.props.dispatch(getNewsFeed());
            // this.getNewsFeed();
            // this.render();
        }
    }

    componentWillUnmount() {
    }

    getNewsFeed = () => {

        const newsFeed = [];
        for (var i = 0; i < this.props.newsFeed.length; i++) {
            var date = new Date(this.props.newsFeed[i].postingDate);
            var publishDate = date.toLocaleString();
            if (this.props.newsFeed[i].question === undefined) {
                newsFeed.push(
                    <div>
                        <br></br>
                        <Status
                            status={this.props.newsFeed[i].description}
                            name={this.props.newsFeed[i].name}
                            date={publishDate}
                        />
                    </div>
                );
            }
            else {
                newsFeed.push(
                    <div>
                        <br></br>
                        <Question
                            question={this.props.newsFeed[i].question}
                            answer={this.props.newsFeed[i].answer}
                            teacherName={this.props.newsFeed[i].teacherName}
                            date={publishDate}
                        />
                    </div>
                );
            }
        }
        return newsFeed;
    }
    render() {
        return (
            <div style={{ width: '100%' }}>
                {/* Here  we need to have a list of questions and st atuses. */}

                {
                    this.props.loading || this.props.loadingStatus ?
                        <div className='sweet-loading'>
                            <ClipLoader
                                css={override}
                                sizeUnit={"px"}
                                size={150}
                                color={'#123abc'}
                                loading={true}
                            />
                        </div>
                        : this.getNewsFeed()
                }
            </div>
        );
    }
}

News.propTypes = {

};

const mapStateToProps = (state) => {
    return {
        // TODO: get user id from state and use it for method 
        // for questions and statuses
        newsFeed: state.news.newsFeed,
        loading: state.news.isLoading,
        loadingStatus: state.teachers.isLoading,
        error: state.news.error
    };
};

// const mapDispatchToProps = (dispatch) => {
//     return {
//     };
// };

export default connect(mapStateToProps)(News);