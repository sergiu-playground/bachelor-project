import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card } from 'react-bootstrap';

class Question extends Component {

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {
        return (
            <Card className="text-center" border="success" style={{ width: '100%' }}>
                <Card.Header>{this.props.teacherName}</Card.Header>
                <Card.Body>
                    <Card.Title>{this.props.question}</Card.Title>
                    <Card.Text>
                        {this.props.answer}
                    </Card.Text>
                </Card.Body>
                <Card.Footer>
                    {this.props.date}</Card.Footer>
            </Card>
        );
    }
}

Question.propTypes = {
    question: PropTypes.string.isRequired,
    answer: PropTypes.string.isRequired,
    teacherName: PropTypes.number.isRequired,
    date: PropTypes.string.isRequired
    //  answererId: PropTypes.number.isRequired

};

// const mapStateToProps = (state) => {
//     return {
//     };
// };

// const mapDispatchToProps = (dispatch) => {
//     return {
//     };
// };

export default
    // connect(mapStateToProps, mapDispatchToProps)(
    Question
    // )
    ;