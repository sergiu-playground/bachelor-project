import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card } from 'react-bootstrap';

class Status extends Component {

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {
        return (
            <Card className="text-left">
                <Card.Header>{this.props.name}</Card.Header>
                <Card.Body>
                    <Card.Title>{this.props.status}</Card.Title>
                    {/* <Card.Text>
                        {this.props.status}
                    </Card.Text>
                    <Button variant="primary">Go somewhere</Button> */}
                </Card.Body>
                <Card.Footer className="text-muted">{this.props.date}</Card.Footer>
            </Card>

        );
    }
}

Status.propTypes = {
    status: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
};

// const mapStateToProps = (state) => {
//     return {
//     };
// };

// const mapDispatchToProps = (dispatch) => {
//     return {
//     };
// };

export default
    // connect(mapStateToProps, mapDispatchToProps)(
    Status
    // )
    ;