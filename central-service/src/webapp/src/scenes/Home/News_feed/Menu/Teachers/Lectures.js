import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { Container, Row, Col, Nav, Table } from 'react-bootstrap';
import Course from './Course';
import { getLecturesByTeacherId } from '../../../../../services/actions/lectureActions';
import { ClipLoader } from 'react-spinners';
import { override } from '../../../../../services/constants';

class Lectures extends Component {
    constructor(props) {
        super(props);

        this.state = {
            content: [],
            key: ''
        }
    }

    componentDidMount() {
        if (this.props.lectures.length > 0) {
            this.handleSelect(this.props.lectures[0].name);
        }
    }
    componentWillUnmount() {
        console.log()

    }

    handleSelect = (eventKey) => {
        this.setState({ key: eventKey });
    }

    getLectures = () => {

        const theLectures = [];
        const result = [];


        for (var i = 0; i < this.props.lectures.length; i++) {
            theLectures.push(
                <Nav.Link eventKey={`${this.props.lectures[i].name}`}>{this.props.lectures[i].name}</Nav.Link>
            )

        }
        result.push(
            <Nav defaultActiveKey={`${this.props.lectures[0].name}`} className="flex-column" onSelect={k => this.handleSelect(k)}>
                {
                    theLectures
                }
            </Nav>
        );

        return result;
    }

    getCourses = (lecture) => {
        // this.setState({ content: [] })

        const result = [];
        const courses = [];

        for (var i = 0; i < this.props.courses.length; i++) {
            if (this.props.courses[i].lectureName === lecture) {
                courses.push(
                    <Course
                        index={i}
                        id={this.props.courses[i].id}
                        name={this.props.courses[i].courseName}
                        lecture={this.props.courses[i].lectureName}
                    >
                    </Course>
                )
            }

        }
        result.push(
            <Table>
                <tbody>
                    {
                        courses
                    }
                </tbody>
            </Table>
        )
        // this.setState({ content: result });
        return result;

    }

    getState = () => {
        return this.getCourses(this.state.key);
        // return this.state.content;
    }
    render() {
        return (
            <Container>
                <br></br>
                <Row>
                    <Col>
                        {
                            this.getLectures()
                        }
                    </Col>
                    <Col>
                        {
                            this.props.waitingForGettingCourses ?
                                <div className='sweet-loading'>
                                    <ClipLoader
                                        css={override}
                                        sizeUnit={"px"}
                                        size={150}
                                        color={'#123abc'}
                                        loading={true}
                                    />
                                </div>
                                :
                                this.getState()
                        }
                    </Col>
                </Row>
            </Container>
        );
    }
}

Lectures.propTypes = {
    teacherId: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => {
    return {
        courses: state.course.teacherCourses,
        lectures: state.lectures.lecturesByTeacherId,
        waitingForGettingCourses: state.course.isLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getTeacherLectures: (teacherId) => {
            dispatch(getLecturesByTeacherId(teacherId));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Lectures);