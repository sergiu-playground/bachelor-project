import React, { Component } from 'react';
import { connect } from "react-redux";
import ApplicationSign from '../ApplicationSign';
import Menu from "../Menu";
import {
    getAllTeachers, resolvedErrorGetAll
} from "../../../../services/actions/teacherActions";
import Container from 'react-bootstrap/Container';
import { CardColumns, Row, Col, Navbar } from 'react-bootstrap';
import UserProfileSign from '../UserProfileSign';
import Teacher from './Teachers/Teacher';
class Teachers extends Component {
    constructor(props) {
        super(props);

        this.getTeachers = this.getTeachers.bind(this);
    }

    getTeachers() {
        const theTeachers = [];
        for (var i = 0; i < this.props.teachersList.length; i++) {
            theTeachers.push(<Teacher
                id={this.props.teachersList[i].id}
                // imgUrl={logo}
                name={this.props.teachersList[i].firstName}
                description={this.props.teachersList[i].description}
                areasOfInterest={this.props.teachersList[i].areasOfInterest}
            />);
        }
        return theTeachers;

    }

    componentDidMount() {
        this.props.getAllTeachers();
    }
    componentWillUnmount() {

    }
    render() {
        return (
            <Container fluid={true} >
                <Navbar bg="white" className="bg-light justify-content-between">
                    <ApplicationSign />
                    {/* <SearchBar /> */}
                    <UserProfileSign />
                </Navbar>
                <Row>
                    <Col xs sm={2}>
                        <Menu />
                    </Col>
                    <Col xs={{ order: 1 }} sm={8}>

                        <CardColumns>
                            {
                                this.getTeachers()
                            }
                        </CardColumns>

                    </Col>
                    {/* {this.props.hasError ?
                            <ErrorMessage text={this.props.errorMessage} callback={() => this.props.onClickErrorOk()} /> :
                            null}
                        {
                            this.props.isAuthenticated ?
                                <Redirect to={{ pathname: '/' }} />
                                : null
                        } */}
                </Row>
            </Container>
        );
    }
}

Teachers.propTypes = {

};

const mapStateToProps = (state) => {
    return {
        teachersList: state.teachers.allTeachers,
        loading: state.teachers.isLoading,
        error: state.teachers.error,
        hasError: state.teachers.hasError,
        errorMessage: state.teachers.errorMessage,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onClickErrorOk: () => { dispatch(resolvedErrorGetAll()) },
        getAllTeachers: () => { dispatch(getAllTeachers()) }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Teachers);