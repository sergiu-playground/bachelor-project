import React, { Component } from 'react';
import { connect } from "react-redux";
import { Card, Container, Navbar,  Nav } from 'react-bootstrap';
import ApplicationSign from '../../ApplicationSign';
import UserProfileSign from '../../UserProfileSign';
import { ClipLoader } from 'react-spinners';
import { override } from '../../../../../services/constants';
import Lectures from './Lectures';
import { getTeacherById } from '../../../../../services/actions/teacherActions';
import Question from '../../News/Question';
import { getLecturesByTeacherId } from '../../../../../services/actions/lectureActions';
import { getTeacherCoursesByTeacherId } from '../../../../../services/actions/courseActions';
import { getTeacherAnsweredQuestionsByTeacherId } from '../../../../../services/actions/questionActions';

class TeacherContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            content: []
        }
    }

    componentDidMount() {
        const teacherId = this.props.match.params.teacherId;
        if (teacherId !== undefined && teacherId !== null) {
            this.props.getTeacher(teacherId);
            this.props.getTeacherLectures(teacherId);
            this.props.getTeacherCourses(teacherId);
            this.props.getTeacherAnsweredQuestions(teacherId);
        }
        this.getAnsweredQuestions();
    }
    componentWillUnmount() { }

    componentDidUpdate(prevProps) {
        if(this.props.answeredQuestions.length !== prevProps.answeredQuestions.length){
        this.getAnsweredQuestions();
        }
    }

    handleSelect(eventKey) {
        switch (eventKey) {
            case "answeredQuestions":
                return this.getAnsweredQuestions();
            case "lectures":
                return this.getLectures();
            case "details":
                return this.getDetails();
            default:
                break;
        }
    }

    getAnsweredQuestions = () => {
        this.setState({ content: [] })
        const result = [];
        for (var i = 0; i < this.props.answeredQuestions.length; i++) {
            var date = new Date(this.props.answeredQuestions[i].postingDate);
            var publishDate = date.toLocaleString();
            result.push(
                <div>
                    <br></br>
                    <Question
                        question={this.props.answeredQuestions[i].question}
                        answer={this.props.answeredQuestions[i].answer}
                        teacherName={this.props.answeredQuestions[i].teacherName}
                        date={publishDate}
                    />
                </div>
            );
        }

        this.setState({ content: result });
    }

    getLectures = () => {
        this.setState({ content: [] })
        const result = [];
        result.push(
            <Lectures
                teacherId={this.props.match.params.teacherId}
            />
        )
        this.setState({ content: result });
    }

    getDetails = () => {
        this.setState({ content: [] });
        var detailsPage = [];
        detailsPage.push(
            <div>
                <br />
                <Card border="light" >
                    <br></br>
                    {/* <div align="center">
                        <Image 
                            src={"logo"}
                            width={80}
                            height={80}
                            roundedCircle />
                    </div> */}
                    <Card.Body>
                        <Card.Title>
                            {
                                this.props.teacher.name
                            }
                        </Card.Title>
                        <Card.Text>
                            <a href={this.props.teacher.website} target="_blank">{this.props.teacher.website}</a>
                            {' '}
                        </Card.Text>
                        <Card.Text>
                            {this.props.teacher.description}
                            {' '}
                        </Card.Text>

                    </Card.Body>
                    <Card.Footer>
                        <small className="text-muted">Areas of interest: {this.props.teacher.areasOfInterest}</small>
                    </Card.Footer>
                </Card>
            </div>
        );
        this.setState({ content: detailsPage });


    }
    render() {
        return (
            <Container fluid={true} >
                <Navbar sticky="top" fixed="top" bg="white" className="bg-light justify-content-between">
                    <ApplicationSign />
                    {/* <SearchBar /> */}
                    <UserProfileSign />
                </Navbar>
                <Nav justify variant="tabs" defaultActiveKey="answeredQuestions" onSelect={k => this.handleSelect(k)}>
                    <Nav.Item>
                        <Nav.Link eventKey="answeredQuestions">Questions</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="lectures">Lectures</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="details">Details</Nav.Link>
                    </Nav.Item>
                </Nav>
                {
                    this.props.waitingForUnansweredQuestions || this.props.waitingForPersistingTheAnswer ?
                        <div className='sweet-loading'>
                            <ClipLoader
                                css={override}
                                sizeUnit={"px"}
                                size={150}
                                color={'#123abc'}
                                loading={true}
                            />
                        </div>
                        :
                        this.state.content
                }
            </Container>);
    }
}

TeacherContent.propTypes = {

};

const mapStateToProps = (state) => {
    return {
        teacher: state.teachers.teacher,
        newsFeed: state.news.newsFeed,
        answeredQuestions: state.questions.answeredQuestions
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getTeacher: (id) => {
            dispatch(getTeacherById(id));
        },
        getTeacherLectures: (teacherId) => {
            dispatch(getLecturesByTeacherId(teacherId));
        },
        getTeacherCourses: (teacherId) =>{
            dispatch(getTeacherCoursesByTeacherId(teacherId))
        },
        getTeacherAnsweredQuestions: (teacherId) =>{
            dispatch(getTeacherAnsweredQuestionsByTeacherId(teacherId))
        }

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TeacherContent);