import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card } from 'react-bootstrap'

class Teacher extends Component {

    componentDidMount() {

    }
    componentWillUnmount() {

    }
    render() {
        return (
            <Card>
                <br />
                <div align="center">
                {/* <a href={`/teachers/${this.props.id}`}>
                    <Image
                        // variant="top"
                        src={this.props.imgUrl}
                        width={80}
                        height={80}
                        roundedCircle
                    />
                    </a> */}
                </div>
                <Card.Body>
                    <Card.Title>
                        <a href={`/teachers/${this.props.id}`}>
                        {
                            this.props.name
                        }</a>
                    </Card.Title>
                    <Card.Text>
                        <a href={this.props.website} target="_blank">{this.props.website}</a>
                        {' '}
                    </Card.Text>
                    <Card.Text>
                        {this.props.description}
                        {' '}
                    </Card.Text>
                </Card.Body>
                <Card.Footer>
                    <small className="text-muted">Areas of interest: {this.props.areasOfInterest}</small>
                </Card.Footer>
            </Card>
        );
    }
}

Teacher.propTypes = {
    id: PropTypes.string.isRequired,
    imgUrl: PropTypes.string,
    name: PropTypes.string.isRequired,
    website: PropTypes.string.isRequired,
    description: PropTypes.string,
    areasOfInterest: PropTypes.string
};

export default Teacher;