import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { Button } from 'react-bootstrap';
import Axios from 'axios';
import { httpApiUrl } from '../../../../../services/constants';

class Course extends Component {
    constructor(props) {
        super(props);
        this.onPressClick = this.onPressClick.bind(this);
    }

    componentDidMount() {

    }
    componentWillUnmount() {

    }
    onPressClick(){
        Axios(`${httpApiUrl}/course/${this.props.id}`, {
            method: 'GET',
            withCredentials: true,
            responseType: 'blob' //Force to receive data in a Blob Format
        })
            .then(response => {
                //Create a Blob from the PDF Stream
                const file = new Blob(
                    [response.data],
                    { type: 'application/pdf' });
                //Build a URL from the file
                const fileURL = URL.createObjectURL(file);
                //Open the URL on new Window
                window.open(fileURL, "_blank");
            })
            .catch(error => {
                console.log(error);
            });
    }
    render() {
        return (
            <tr>
                <td>{this.props.name}</td>
                <td>
                    <Button
                        variant="primary"
                        onClick={this.onPressClick}
                    >
                        Show Course
                            </Button>
                </td>
            </tr>
        );
    }
}

Course.propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Course);