import React, { Component } from 'react';
import { connect } from "react-redux";
import { Nav } from 'react-bootstrap';
import UnansweredQuestion from './TeacherProf/Questions/Question';
import Status from './TeacherProf/Feed/Status';
import { getFeedForTeacher, getTeacherById } from '../../services/actions/teacherActions';
import TeachingPage from './TeacherProf/teaching/TeachingPage';
import DetailsPage from './TeacherProf/Details/DetailsPage';
import { ClipLoader } from 'react-spinners';
import { override } from '../../services/constants';
import { getAllUnansweredQuestions } from '../../services/actions/questionActions';
import Question from '../Home/News_feed/News/Question';

class TeacherProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            content: []
        };

        this.handleSelect = this.handleSelect.bind(this);
        this.getFeed = this.getFeed.bind(this);
        this.getCoursePage = this.getCoursePage.bind(this);
        this.getDetails = this.getDetails.bind(this);
    }

    componentDidMount() {
        this.props.getUnansweredQuestions();
        this.props.getFeedForTeacher();
        this.handleSelect("unansweredQuestions");
        this.props.getTeacher(this.props.id);

    }
    componentWillUnmount() {

    }
    handleSelect(eventKey) {
        switch (eventKey) {
            case "unansweredQuestions":
                return this.getUnansweredQuestions();
            case "teaching":
                return this.getCoursePage();
            case "feed":
                return this.getFeed();
            case "details":
                return this.getDetails();
            default:
                break;
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.waitingForPersistingTheAnswer !== this.props.waitingForPersistingTheAnswer &&
            prevProps.waitingForPersistingTheAnswer === true &&
            prevProps.waitingForUnansweredQuestions === false) {

                this.props.getUnansweredQuestions();
        }

        if(prevProps.unansweredQuestions.length !== this.props.unansweredQuestions.length)
        {
            this.getUnansweredQuestions();
        }
    }

    getUnansweredQuestions = () => {
        this.setState({ content: [] })
        const theQuestions = [];
        for (var i = 0; i < this.props.unansweredQuestions.length; i++) {
            var date = new Date(this.props.unansweredQuestions[i].postingDate);
            var publishDate = date.toLocaleString();
            theQuestions.push(
                <div>
                    <br></br>
                    <UnansweredQuestion
                        questionId={this.props.unansweredQuestions[i].id}
                        question={this.props.unansweredQuestions[i].question}
                        date={publishDate}
                    ></UnansweredQuestion>
                </div>
            );
        }
        this.setState({ content: theQuestions });
    }

    getFeed() {
        this.setState({ content: [] })
        var feed = [];
        for (var i = 0; i < this.props.feed.length; i++) {
            var date = new Date(this.props.feed[i].postingDate);
            var publishDate = date.toLocaleString();
            if (this.props.feed[i].question === undefined) {
                feed.push(
                    <div>
                        <br></br>
                        <Status
                            teacherName={this.props.feed[i].name}
                            status={this.props.feed[i].description}
                            date={publishDate}
                        />
                    </div>
                );
            }
            else {
                feed.push(
                    <div>
                        <br></br>
                        <Question
                            question={this.props.feed[i].question}
                            answer={this.props.feed[i].answer}
                            teacherName={this.props.feed[i].teacherName}
                            date={publishDate}
                        />
                    </div>
                );
            }
        }

        this.setState({ content: feed })

    }

    getCoursePage() {
        this.setState({ content: [] });
        var coursePage = [];
        coursePage.push(<TeachingPage></TeachingPage>);
        this.setState({ content: coursePage });
    }

    getDetails() {
        this.setState({ content: [] });
        var detailsPage = [];
        detailsPage.push(
            <div>
                <br />
                <DetailsPage></DetailsPage>
            </div>
        );
        this.setState({ content: detailsPage });

    }
    
    render() {
        return (<div>
            <Nav justify variant="tabs" defaultActiveKey="unansweredQuestions" onSelect={k => this.handleSelect(k)}>
                <Nav.Item>
                    <Nav.Link eventKey="unansweredQuestions">Questions</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="teaching">Teaching</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="feed">Feed</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="details">Details</Nav.Link>
                </Nav.Item>
            </Nav>
            {
                this.props.waitingForUnansweredQuestions || this.props.waitingForPersistingTheAnswer ?
                    <div className='sweet-loading'>
                        <ClipLoader
                            css={override}
                            sizeUnit={"px"}
                            size={150}
                            color={'#123abc'}
                            loading={true}
                        />
                    </div>
                    :
                    this.state.content
            }
        </div>);
    }
}

TeacherProfile.propTypes = {
};

const mapStateToProps = (state) => {
    return {
        id: state.login.userId,
        unansweredQuestions: state.questions.unansweredQuestions,
        feed: state.teachers.feed,
        waitingForUnansweredQuestions: state.questions.isLoading,
        waitingForPersistingTheAnswer: state.questions.waitingForPersistingTheAnswer,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getUnansweredQuestions: () => {
            dispatch(getAllUnansweredQuestions())
        },
        getFeedForTeacher: () => {
            dispatch(getFeedForTeacher())
        },
        getTeacher: (id) => {
            dispatch(getTeacherById(id))
        }
        
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TeacherProfile);