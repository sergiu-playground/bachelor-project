import React, { Component } from 'react';
import { connect } from "react-redux";
import { Button, ButtonGroup, Nav, Table } from 'react-bootstrap';
import AddCourse from './AddCourse';
// import AddSeminar from './AddSeminar';
// import AddLaboratory from './AddLaboratory';
import Course from './Course';
import { getTeacherCourses } from '../../../../services/actions/courseActions';
import { ClipLoader } from 'react-spinners';
import { override } from '../../../../services/constants';

class TeachingPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalCourseShow: false,
      modalSeminarShow: false,
      modalLaboratoryShow: false
    };
  }

  componentDidMount() {
    this.props.getTeacherCourses();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.waitingForSavingCourse === true) {
      this.props.getTeacherCourses();
    }
  }
  handleSelect(eventKey) {
    switch (eventKey) {
      case "courses":
        return this.getCourses();
      case "seminars":
        // return this.getCoursePage();
        //todo: create seminars flow
        break;
      case "laboratories":
        //todo: create laboratories flow
        break;
      default:
        break;
      // code block
    }
  }

  getCourses = () => {
    const theCourses = [];
    const result = [];
    for (var i = 0; i < this.props.courses.length; i++) {
      theCourses.push(
        <Course
          index={i}
          id={this.props.courses[i].id}
          name={this.props.courses[i].courseName}
          lecture={this.props.courses[i].lectureName}
        >
        </Course>
      );
    }
    result.push(
      <Table>
        <tbody>
          {
            theCourses
          }
        </tbody>
      </Table>
    )
    return result;
  }

  render() {
    let modalClose = () => this.setState({
      modalCourseShow: false,
      modalSeminarShow: false,
      modalLaboratoryShow: false,
    });
    return (
      <div>
        <div className="d-flex flex-column">
          <ButtonGroup className="mt-3">
            <Button
              variant="primary"
              onClick={() => this.setState({ modalCourseShow: true })}
            >
              Add Course
            </Button>
            {/* <Button
              variant="primary"
              onClick={() => this.setState({ modalSeminarShow: true })}
              disabled={true}
            >
              Add Seminar
            </Button> */}
            {/* <Button
              variant="primary"
              onClick={() => this.setState({ modalLaboratoryShow: true })}
              disabled={true}
            >
              Add Laboratory
            </Button> */}

            <AddCourse
              show={this.state.modalCourseShow}
              onHide={modalClose}
            />
            {/* <AddSeminar
              show={this.state.modalSeminarShow}
              onHide={modalClose}
            />
            <AddLaboratory
              show={this.state.modalLaboratoryShow}
              onHide={modalClose}
            /> */}
          </ButtonGroup>
        </div>
        <br />
        <br />
        <Nav justify variant="tabs" defaultActiveKey="courses" onSelect={k => this.handleSelect(k)}>
          <Nav.Item>
            <Nav.Link eventKey="courses">Courses</Nav.Link>
          </Nav.Item>
          {/* <Nav.Item>
            <Nav.Link eventKey="seminars" disabled>Seminars</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey="laboratories" disabled>Laboratories</Nav.Link>
          </Nav.Item> */}
        </Nav>
        {
          this.props.waitingForSavingCourse || this.props.waitingForReceivingCourses ?
            <div className='sweet-loading'>
              <ClipLoader
                css={override}
                sizeUnit={"px"}
                size={150}
                color={'#123abc'}
                loading={true}
              />
            </div>
            :
            this.getCourses()
        }
      </div>

      // TODO: dupa buton trebuie sa fie o lista cu lecturile profesorului
    );
  }
}

TeachingPage.propTypes = {

};

const mapStateToProps = (state) => {
  return {
    courses: state.course.teacherCourses,
    waitingForReceivingCourses: state.course.isLoading,
    waitingForSavingCourse: state.course.waitingForSavingCourse,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getTeacherCourses: () => {
      dispatch(getTeacherCourses())
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TeachingPage);