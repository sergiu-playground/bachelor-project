import React, { Component } from 'react';
import { connect } from "react-redux";
import Axios from 'axios';
import { httpApiUrl } from '../../../../services/constants';

class CourseContent extends Component {

    componentDidMount() {
        Axios(`${httpApiUrl}/course/${this.props.match.params.id}`, {
            method: 'GET',
            withCredentials: true,
            responseType: 'blob' //Force to receive data in a Blob Format
        })
            .then(response => {
                //Create a Blob from the PDF Stream
                // const file = new Blob(
                //     [response.data],
                //     { type: 'application/pdf' });
                //Build a URL from the file
                // const fileURL = URL.createObjectURL(file);
                //Open the URL on n ew Window
                window.open(`${httpApiUrl}/course/${this.props.match.params.id}`, "_self");
            })
            .catch(error => {
                console.log(error);
            });
    }
    componentWillUnmount() {

    }
    render() {
        return (<div>
            <h1>
                Redirect to the course content page...
            </h1>
        </div>);
    }
}

CourseContent.propTypes = {

};

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CourseContent);