import React, { Component } from 'react';
import { connect } from "react-redux";
import { Modal, Button, Form, Col, Row } from 'react-bootstrap';
import { saveCourse } from '../../../../services/actions/courseActions';

class AddCourse extends Component {
    constructor(props) {
        super(props);

        this.state = {
            courseName: '',
            file: null
        };
        this.handleOnChange = this.handleOnChange.bind(this);
        this.onPressClick = this.onPressClick.bind(this);
    }

    componentDidMount() {

    }
    componentWillUnmount() {

    }

    onPressClick() {
        let myForm = document.getElementById('myForm');
        let formData = new FormData(myForm);
        this.props.onTodoClick(formData);
        this.props.onHide();
    }

    handleOnChange(e) {
        this.setState({ [e.target.name]: e.target.value });

    }
    render() {
        return (
            <Modal
                {...this.props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Add Course
            </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form
                        id="myForm"
                        name="myForm" >
                        <Form.Group as={Row} controlId="formHorizontalEmail">
                            <Form.Label column sm={2}>
                                Course name
                        </Form.Label>
                            <Col sm={10}>
                                <Form.Control
                                    type="text"
                                    placeholder="Course name"
                                    name="courseName"
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="formHorizontalEmail">
                            <Form.Label column sm={2}>
                                Lecture
                        </Form.Label>
                            <Col sm={10}>
                                <Form.Control
                                    type="text"
                                    placeholder="Lecture"
                                    name="lectureName"
                                    />
                            </Col>
                        </Form.Group>
                        <Form.Control
                            type="file"
                            name="file"
                        />
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-secondary"
                        onClick={this.onPressClick}
                    >
                        Save
                    </Button>
                    <Button onClick={this.props.onHide}>Close</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

AddCourse.propTypes = {

};

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onTodoClick: (courseData) => {
            dispatch(saveCourse(courseData))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddCourse);