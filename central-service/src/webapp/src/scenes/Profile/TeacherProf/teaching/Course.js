import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { Button } from 'react-bootstrap';
import Axios from 'axios';
import { httpApiUrl } from '../../../../services/constants';
import ModifyCourse from './ModifyCourse';

class Course extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalCourseModifyShow: false,
        };
    }

    componentDidMount() {

    }
    componentWillUnmount() {

    }

    showCourseContent = () => {
        Axios(`${httpApiUrl}/course/${this.props.id}`, {
            method: 'GET',
            withCredentials: true,
            responseType: 'blob' //Force to receive data in a Blob Format
        })
            .then(response => {
                //Create a Blob from the PDF Stream
                const file = new Blob(
                    [response.data],
                    { type: 'application/pdf' });
                //Build a URL from the file
                const fileURL = URL.createObjectURL(file);
                //Open the URL on new Window
                window.open(fileURL, "_blank");
            })
            .catch(error => {
                console.log(error);
            });
    }

    modifyCourse = () => {

    }
    render() {
        if (this.state.modalCourseModifyShow) {
            let modalClose = () => this.setState({
                modalCourseModifyShow: false,
            });
            return (
                <div>
                    <div className="d-flex flex-column">
                        <ModifyCourse
                            show={this.state.modalCourseModifyShow}
                            onHide={modalClose}
                            id={this.props.id}
                        />
                    </div>
                </div>
            )
        }
        return (
            <tr>
                <td>{this.props.index}</td>
                <td>{this.props.name}</td>
                <td>{this.props.lecture}</td>
                <td>
                    <Button
                        variant="primary"
                        onClick={this.showCourseContent}
                    >
                        Show Course
                            </Button>
                </td>
                <td>
                    <Button
                        variant="primary"
                        onClick={() => this.setState({ modalCourseModifyShow: true })}
                    >
                        Modify
                            </Button>
                </td>
                {/* <td>
                    <Button
                        variant="danger"
                    >
                        Delete
                            </Button>
                </td> */}
            </tr>
        );
    }
}

Course.propTypes = {
    id: PropTypes.string.isRequired,
    index: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    lecture: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Course);