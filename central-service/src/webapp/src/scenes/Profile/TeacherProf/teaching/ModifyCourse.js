import React, { Component } from 'react';
import { connect } from "react-redux";
import {Form, Modal, Col, Row, Button } from 'react-bootstrap';
import { update } from '../../../../services/actions/courseActions';

class ModifyCourse extends Component {

    componentDidMount() {

    }
    componentWillUnmount() {

    }

    onPressClick = () => {
        let myForm = document.getElementById('modifyForm');
        let formData = new FormData(myForm);
        this.props.updateCourse(formData, this.props.id);
        this.props.onHide();
    }

    handleOnChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });

    }

    render() {
        return (
            <Modal
                {...this.props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Modify Course
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <Form
                        id="modifyForm"
                        name="modifyForm" >
                        
                        <Form.Group as={Row} controlId="formHorizontalEmail">
                            <Form.Label column sm={2}>
                                Course name
                        </Form.Label>
                            <Col sm={10}>
                                <Form.Control
                                    type="text"
                                    placeholder="Course name"
                                    name="courseName"
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="formHorizontalEmail">
                            <Form.Label column sm={2}>
                                Lecture
                        </Form.Label>
                            <Col sm={10}>
                                <Form.Control
                                    type="text"
                                    placeholder="Lecture"
                                    name="lectureName"
                                    />
                            </Col>
                        </Form.Group>
                        <Form.Control
                            type="file"
                            name="file"
                        />
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-secondary"
                        onClick={this.onPressClick}
                    >
                        Update
                    </Button>
                    <Button onClick={this.props.onHide}>Close</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

ModifyCourse.propTypes = {

};

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateCourse: (formData, courseId) => {
            dispatch(update(formData, courseId))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModifyCourse);