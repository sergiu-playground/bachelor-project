import React, { Component } from 'react';
import { connect } from "react-redux";
import { Modal, Button, Form, Col, Row } from 'react-bootstrap';

class AddSeminar extends Component {

    componentDidMount() {

    }
    componentWillUnmount() {

    }

    onPressClick() {
        let myForm = document.getElementById('myFormSeminar');
        let formData = new FormData(myForm);
        this.props.onTodoClick(formData)
    }
    render() {
        return (
            <Modal
                {...this.props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Add Seminar
        </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form
                        id="myFormSeminar"
                        name="myFormSeminar" >
                        <Form.Group as={Row} controlId="formHorizontalEmail">
                            <Form.Label column sm={2}>
                                Seminar name
                    </Form.Label>
                            <Col sm={10}>
                                <Form.Control
                                    type="text"
                                    placeholder="Seminar name"
                                    name="seminarName"
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="formHorizontalEmail">
                            <Form.Label column sm={2}>
                                Lecture
                    </Form.Label>
                            <Col sm={10}>
                                <Form.Control
                                    type="text"
                                    placeholder="Lecture"
                                    name="lectureName" />
                            </Col>
                        </Form.Group>
                        <Form.Control
                            type="file"
                            name="file"
                        />
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-secondary"
                        onClick={this.onPressClick}
                    >
                        Save
                </Button>
                    <Button onClick={this.props.onHide}>Close</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

AddSeminar.propTypes = {

};

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddSeminar);