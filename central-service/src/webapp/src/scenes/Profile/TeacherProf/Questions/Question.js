import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { Card, InputGroup, FormControl, Button } from 'react-bootstrap';
import { answerQuestion } from '../../../../services/actions/questionActions';

class Question extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            answer: '',
        }
        
        this.handleOnChange = this.handleOnChange.bind(this);
        this.onPressClick = this.onPressClick.bind(this);
    }

    componentDidMount() {
        this.setState({ id: this.props.questionId })
    }
    componentWillUnmount() {

    }
    onPressClick() {
        this.props.onTodoClick(this.state)
    }
    handleOnChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    render() {
        return (
            <Card className="text-left" border="success" style={{ width: '100%' }}>
                <Card.Header>{this.props.question}</Card.Header>
                <Card.Body>
                    <Card.Title></Card.Title>
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text>Answer</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            as="textarea"
                            aria-label="With textarea"
                            name="answer"
                            value={this.state.answer}
                            onChange={this.handleOnChange}
                        />
                        <InputGroup.Append>
                            <Button
                                variant="outline-secondary"
                                type="reset"
                                onClick={this.onPressClick}
                            >
                                Publish
                        </Button>
                        </InputGroup.Append>
                    </InputGroup>
                    <Card.Footer>{this.props.date}</Card.Footer>
                </Card.Body>
            </Card>
        );
    }
}

Question.propTypes = {
    questionId: PropTypes.string.isRequired,
    question: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,

};

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onTodoClick: (data) => {
            dispatch(answerQuestion(data))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Question);