import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { Card } from 'react-bootstrap';

class InfoCardBox extends Component {

    componentDidMount() {

    }
    componentWillUnmount() {

    }
    render() {
        return (
            <Card>
                <br></br>
                {/* <div align="center">
                    <Image
                        src={logo}
                        width={80}
                        height={80}
                        roundedCircle />
                </div> */}
                <Card.Body>
                    <Card.Title>
                        {
                            this.props.name
                        }
                    </Card.Title>
                    <Card.Text>
                        <a href={this.props.website} target="_blank">{this.props.website}</a>
                        {' '}
                    </Card.Text>
                    <Card.Text>
                        {this.props.description}
                        {' '}
                    </Card.Text>

                </Card.Body>
                <Card.Footer>
                    <small className="text-muted">Areas of interest: {this.props.areasOfInterest}</small>
                </Card.Footer>
            {/* <Image src={logo} roundedCircle /> */}
            </Card>
        );
    }
}

InfoCardBox.propTypes = {
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    areasOfInterest: PropTypes.string.isRequired,
    website: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(InfoCardBox);