import React, { Component } from 'react';
import { connect } from "react-redux";
import { Button, Form, Col, Row, Container } from 'react-bootstrap';
import { ClipLoader } from 'react-spinners';
import { override } from '../../../../services/constants';

import { update, getTeacherById } from '../../../../services/actions/teacherActions';
import InfoCardBox from './InfoCardBox';

class DetailsPage extends Component {

    componentDidUpdate(prevProps) {
        if (prevProps.waitingForUpdatingTeacherInfo === true) {
            this.props.getTeacher(this.props.id);
        }
    }

    onPressClick = () => {
        let myForm = document.getElementById('teacherDetailsForm');
        let formData = new FormData(myForm);
        this.props.onTodoClick(formData);
    }

    getBoxWithPersonalData = () => {
        var infoCardBox = [];
        infoCardBox.push(
            <Col>
                <InfoCardBox
                    name={this.props.teacher.name}
                    description={this.props.teacher.description}
                    areasOfInterest={this.props.teacher.areasOfInterest}
                    website={this.props.teacher.website}
                ></InfoCardBox>
            </Col>
        );
        return infoCardBox;
    }

    render() {
        return (
            <Container>
                <Row >
                    <Col >
                        <Form
                            id="teacherDetailsForm"
                            name="teacherDetailsForm" >
                            <Form.Group controlId="formHorizontalEmail">
                                <Form.Label >
                                    Name
                        </Form.Label>
                                <Form.Control
                                    type="text"
                                    name="name"
                                    defaultValue={this.props.teacher.name}
                                />
                            </Form.Group>
                            {/* <Form.Group controlId="formHorizontalEmail">
                                <Form.Label>
                                    Add Picture
                            </Form.Label>
                                <Form.Control
                                    type="file"
                                    name="picture"
                                />
                            </Form.Group> */}
                            <Form.Group controlId="formHorizontalEmail">
                                <Form.Label >
                                    Description
                            </Form.Label>
                                <Form.Control
                                    as="textarea"
                                    placeholder="Example: email"
                                    name="description"
                                    defaultValue={this.props.teacher.description}
                                />
                            </Form.Group>
                            <Form.Group controlId="formHorizontalEmail">
                                <Form.Label>
                                    Website
                            </Form.Label>
                                <Row fluid={true}>
                                    <Col sm={4}>
                                        <Form.Group controlId="formGridState">
                                            <Form.Control as="select"
                                                name="websitePrefix"
                                            >
                                                <option>http://</option>
                                                <option>https://</option>
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>
                                    <Col sm={8}>
                                        <Form.Control
                                            type="text"
                                            name="website"
                                        />
                                    </Col>
                                </Row>
                            </Form.Group>
                            <Form.Group controlId="formHorizontalEmail">
                                <Form.Label >
                                    Areas of interest
                                </Form.Label>
                                <Form.Control
                                    as="textarea"
                                    name="areasOfInterest"
                                    placeholder="Example: Artificial Intelligence, Game development, etc. "
                                    defaultValue={this.props.teacher.areasOfInterest}
                                />
                            </Form.Group>
                        </Form>
                        <Button
                            variant="outline-secondary"
                            type="button"
                            onClick={this.onPressClick}
                        >
                            Update details
                    </Button>
                    </Col>
                    {
                        this.props.waitingForUpdatingTeacherInfo ?
                            <div className='sweet-loading'>
                                <ClipLoader
                                    css={override}
                                    sizeUnit={"px"}
                                    size={150}
                                    color={'#123abc'}
                                    loading={true}
                                />
                            </div>
                            :
                            this.getBoxWithPersonalData()
                    }
                </Row>

            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        id: state.login.userId,
        teacher: state.teachers.teacher,
        waitingForUpdatingTeacherInfo: state.teachers.waitingForUpdateTeacherData,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onTodoClick: (teacher) => {
            dispatch(update(teacher))
        },
        getTeacher: (id) => {
            dispatch(getTeacherById(id))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailsPage);