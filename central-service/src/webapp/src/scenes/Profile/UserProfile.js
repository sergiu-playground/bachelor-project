import React, { Component } from 'react';
import { connect } from "react-redux";
import { Container, Navbar, Row, Col } from 'react-bootstrap';
import ApplicationSign from '../Home/News_feed/ApplicationSign';
import UserProfileSign from '../Home/News_feed/UserProfileSign';
import Menu from '../Home/News_feed/Menu';
import TeacherProfile from './TeacherProfile';
import StudentProfile from './StudentProfile';

class UserProfile extends Component {
    componentDidMount() {

    }
    componentWillUnmount() {

    }
    render() {
        return (<Container fluid={true} >
            <Navbar sticky="top" fixed="top" bg="white" className="bg-light justify-content-between">
                <ApplicationSign />
                {/* <SearchBar /> */}
                <UserProfileSign />
            </Navbar>
            <Row>
                <Col xs sm={2}>
                    <Menu />
                </Col>
                <Col lg={{ order: 1 }} sm={8}>
                    {/* TODO: Fix the errors when the connection is down. */}
                    {
                        this.props.userRole === "ROLE_TEACHER" ?
                            <Col xs={{ order: 1 }}>
                                <TeacherProfile />
                            </Col>
                            :
                            <Col xs={{ order: 1 }}>
                                <StudentProfile />
                            </Col>
                    }
                </Col>
            </Row>
        </Container>);
    }
}

UserProfile.propTypes = {

};

const mapStateToProps = (state) => {
    return {
        userRole: state.login.userRole
    };
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);