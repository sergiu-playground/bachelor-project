import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './ErrorMessage.scss';

class ErrorMessage extends Component {

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {
        return (<div className='error-message'>
            <div className='error-message-inner'>
                <h1>{this.props.text}</h1>
                <button onClick={this.props.callback}>Ok</button>
            </div>
        </div>);
    }
}

ErrorMessage.propTypes = {
    text: PropTypes.string.isRequired,
    callback: PropTypes.func.isRequired
};

export default ErrorMessage;