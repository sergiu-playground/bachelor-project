import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import configureStore from "./services/store/configureStore";
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import Routes from './routes';
import { PersistGate } from 'redux-persist/integration/react'

const store = configureStore();
ReactDOM.render(
    <Provider store={store.store}>
        <PersistGate loading={null} persistor={store.persistor}>
            <BrowserRouter>
                <Routes />
            </BrowserRouter>
        </PersistGate>
    </Provider>,
    document.getElementById('root'));
registerServiceWorker();
