package com.feed.repository;

import com.feed.domain.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Repository
public interface StatusRepository extends JpaRepository<Status, Long> {
    List<Status> findAllByUserIdOrderByIdDesc(@RequestParam(name = "userId") String userId);

    List<Status> findAllByOrderByIdDesc();
}
