package com.feed.repository;

import com.feed.domain.Lecture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

@Repository
public interface LectureRepository extends JpaRepository<Lecture, Long> {

    Lecture findByName(@RequestParam(name = "name")String name);
}
