package com.feed.repository;

import com.feed.domain.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {

    List<Question> findAllByAnswererIdAndAnswerNotNullOrderByIdDesc(@RequestParam(name = "answererId") String answererId);

    List<Question> findAllByAnswerNotNullOrderByIdDesc();

    List<Question> findAllByAnswererIdAndAnswerNullOrderByIdDesc(@RequestParam(name = "answererId") String answererId);
}