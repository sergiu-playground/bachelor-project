package com.feed.domain.dto;

import com.feed.domain.Course;

public class CourseConverter {

    public static Course getCourseFromCourseDTO(final CourseDTO courseDTO) {
        Course course = new Course();
        course.setTeacherId(courseDTO.getTeacherId());
        course.setName(courseDTO.getCourseName());
        return course;
    }

    public static CourseDTO getCourseDTOFromCourse(final Course course) {
        CourseDTO courseDTO = new CourseDTO();
        courseDTO.setLectureName(course.getLecture().getName());
        courseDTO.setTeacherId(course.getTeacherId());
        courseDTO.setCourseName(course.getName());
        courseDTO.setId(course.getId().toString());
        courseDTO.setContentType(course.getContentType());
        courseDTO.setDocument(course.getDocument());

        return courseDTO;
    }
}