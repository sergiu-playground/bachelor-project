package com.feed.domain.dto;

import java.io.Serializable;
import java.util.Date;

public class QuestionDTO implements Serializable {

    private String question;
    private String answer;
    private String questionerId;
    private String answererId;
    private String teacherName;
    private Date postingDate;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuestionerId() {
        return questionerId;
    }

    public void setQuestionerId(String questionerId) {
        this.questionerId = questionerId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswererId() {
        return answererId;
    }

    public void setAnswererId(String answererId) {
        this.answererId = answererId;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public Date getPostingDate() {
        return (Date) postingDate.clone();
    }

    public void setPostingDate(Date postingDate) {
        this.postingDate = (Date) postingDate.clone();
    }
}