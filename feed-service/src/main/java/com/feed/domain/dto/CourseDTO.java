package com.feed.domain.dto;

import java.io.Serializable;
import java.util.Arrays;

public class CourseDTO implements Serializable {

    private String id;
    private String teacherId;
    private String courseName;
    private String lectureName;
    private String contentType;
    private byte[] document;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLectureName() {
        return lectureName;
    }

    public void setLectureName(String lectureName) {
        this.lectureName = lectureName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public byte[] getDocument() {
        return Arrays.copyOf(document, document.length);
    }

    public void setDocument(byte[] document) {
        this.document = Arrays.copyOf(document, document.length);
    }
}