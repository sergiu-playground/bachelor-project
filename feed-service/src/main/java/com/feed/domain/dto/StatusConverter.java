package com.feed.domain.dto;

import com.feed.domain.Status;

public class StatusConverter {

    public static Status getStatusFromStatusDTO(final StatusDTO statusDTO) {
        Status status = new Status();
        status.setDescription(statusDTO.getDescription());
        status.setUserId(statusDTO.getUserId());
        status.setPostingDate(statusDTO.getPostingDate());
        status.setName(statusDTO.getName());
        return status;
    }
}
