package com.feed.domain.dto;

import java.util.Date;

public class StatusDTO {

    private String description;
    private Date postingDate;
    private String userId;
    private String name;

    public StatusDTO() {
    }

    public StatusDTO(String description, Date postingDate) {
        this.description = description;
        this.postingDate = (Date) postingDate.clone();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPostingDate() {
        return (Date) postingDate.clone();
    }

    public void setPostingDate(Date postingDate) {
        this.postingDate = (Date) postingDate.clone();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
