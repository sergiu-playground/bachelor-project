package com.feed.domain.dto;

import com.feed.domain.Lecture;

public class LectureConverter {

    public static Lecture getLectureFromLectureDTO(final LectureDTO lectureDTO) {

        Lecture newLecture = new Lecture();

        newLecture.setName(lectureDTO.getName());

        return newLecture;
    }
}
