package com.feed.domain.dto;

import com.feed.domain.Question;

public class QuestionConverter {

    private QuestionConverter() {
    }

    public static Question getQuestionFromQuestionDTO(final QuestionDTO questionDTO) {
        Question newQuestion = new Question();

        newQuestion.setQuestion(questionDTO.getQuestion());
        newQuestion.setAnswer(questionDTO.getAnswer());
        newQuestion.setAnswererId(questionDTO.getAnswererId());
        newQuestion.setQuestionerId(questionDTO.getQuestionerId());
        newQuestion.setTeacherName(questionDTO.getTeacherName());
        newQuestion.setPostingDate(questionDTO.getPostingDate());

        return newQuestion;
    }

    public static QuestionDTO getQuestionDTOFromQuestion(final Question question) {
        QuestionDTO newQuestionDTO = new QuestionDTO();

        newQuestionDTO.setQuestion(question.getQuestion());
        newQuestionDTO.setAnswer(question.getAnswer());

        return newQuestionDTO;
    }
}
