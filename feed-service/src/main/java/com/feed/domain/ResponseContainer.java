package com.feed.domain;

import java.io.Serializable;

public class ResponseContainer implements Serializable {

    private boolean successful;
    private Object data;
    private Object error;

    public ResponseContainer() {
    }

    public ResponseContainer(boolean successful, Object data, Object error) {
        this.successful = successful;
        this.data = data;
        this.error = error;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }
}
