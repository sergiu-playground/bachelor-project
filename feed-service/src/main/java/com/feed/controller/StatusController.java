package com.feed.controller;

import com.feed.domain.dto.StatusDTO;
import com.feed.service.StatusService;
import com.feed.domain.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/status")
public class StatusController {

    @Autowired
    private StatusService statusService;

    @RequestMapping(value = "/save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity postNewStatus(@RequestBody StatusDTO statusDTO) {
        Status newStatus = statusService.create(statusDTO);

        return new ResponseEntity<>(newStatus, HttpStatus.OK);
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public ResponseEntity getStatus(@PathVariable(name = "userId") Long userId) {

        List<Status> statuses = statusService.getAllByUserId(userId);
        return new ResponseEntity<>(statuses, HttpStatus.OK);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllStatusesFromTeachers() {

        List<Status> statuses = statusService.getAllStatusesFromTeachers();
        return new ResponseEntity<>(statuses, HttpStatus.OK);
    }

}
