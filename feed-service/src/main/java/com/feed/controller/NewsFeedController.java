package com.feed.controller;

import com.feed.service.NewsFeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
@RequestMapping("/api/newsFeed")
public class NewsFeedController {

    @Autowired
    private NewsFeedService newsFeedService;

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getNewsFeed() {

        List<Object> feed = newsFeedService.getNewsFeed();
        return new ResponseEntity<>(feed, HttpStatus.OK);

    }

}