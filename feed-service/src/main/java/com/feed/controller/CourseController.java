package com.feed.controller;

import com.feed.domain.dto.CourseDTO;
import com.feed.service.CourseService;
import com.feed.domain.Course;
import com.feed.domain.ResponseContainer;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(value = "/api/course")
public class CourseController {

    @Autowired
    private CourseService courseService;

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ResponseEntity save(@RequestPart(value = "courseDto") CourseDTO courseDTO,
                               @RequestParam(value = "file") String file, @RequestParam(value = "contentType") String contentType) {

        byte[] fileBytes = Base64.decodeBase64(file);
        Course newCourse = courseService.create(courseDTO, fileBytes, contentType);
        ResponseContainer responseContainer = new ResponseContainer(true, newCourse.getName(), null);

        return new ResponseEntity(responseContainer, HttpStatus.OK);
    }

    @RequestMapping(value = "/{teacherId}/{lecture}/{courseName}", method = RequestMethod.GET)
    public ResponseEntity getCourse(@PathVariable("teacherId") String teacherId,
                                    @PathVariable("lecture") String lectureName,
                                    @PathVariable("courseName") String courseName) throws Exception {

        Course newCourse = courseService.getCourse(teacherId, lectureName, courseName);
        byte[] responseDocument = newCourse.getDocument();

        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.put("Content-Type", Collections.singletonList(newCourse.getContentType()));

        return new ResponseEntity(responseDocument, headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity getCourseContent(@PathVariable("id") String id) throws Exception {

        CourseDTO newCourse = courseService.getCourseContent(id);
        return new ResponseEntity(newCourse, HttpStatus.OK);
    }

    @RequestMapping(value = "/getTeacherCourses/{teacherId}", method = RequestMethod.GET)
    public ResponseEntity getTeacherCourses(@PathVariable("teacherId") String teacherId) throws Exception {

        List<CourseDTO> courseDTOS = courseService.getTeacherCourses(teacherId);

        return new ResponseEntity(courseDTOS, HttpStatus.OK);
    }
}