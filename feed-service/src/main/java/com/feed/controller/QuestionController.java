package com.feed.controller;

import com.feed.domain.dto.QuestionDTO;
import com.feed.service.QuestionService;
import com.feed.domain.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/question")
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @RequestMapping(value = "/save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addQuestion(@RequestBody QuestionDTO questionDTO) {
        Question question = questionService.create(questionDTO);

        return new ResponseEntity(question.getQuestion(), HttpStatus.OK);
    }

    //This is for teacher profile and students
    @RequestMapping(value = "/getAll/{teacherId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllByTeacherId(@PathVariable("teacherId") String teacherId) {
        List<Question> questionList = questionService.getAllByTeacherIdAndAnswerNotNull(teacherId);
        return new ResponseEntity(questionList, HttpStatus.OK);
    }

    //This is just for teacher profile, questions with no answer.
    @RequestMapping(value = "/{teacherId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllQuestionForAnswered(@PathVariable("teacherId") String teacherName) {
        List<Question> questionList = questionService.getAllByTeacherIdAndAnswerNull(teacherName);
        return new ResponseEntity(questionList, HttpStatus.OK);
    }

    //This is for newsFeed
    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllQuestions() {
        List<Question> questionList = questionService.getAllWithAnswerOrdered();
        return new ResponseEntity(questionList, HttpStatus.OK);
    }
    //This is for newsFeed
    @RequestMapping(value = "/answerQuestion", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity answerQuestion(@RequestBody Question question) {
        questionService.answerQuestion(question);
        return new ResponseEntity( HttpStatus.OK);
    }
}
