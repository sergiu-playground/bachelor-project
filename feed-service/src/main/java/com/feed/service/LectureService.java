package com.feed.service;

import com.feed.domain.Course;
import com.feed.domain.Lecture;
import com.feed.domain.dto.CourseDTO;
import com.feed.domain.dto.LectureDTO;
import com.feed.repository.LectureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.feed.domain.dto.CourseConverter.getCourseDTOFromCourse;
import static com.feed.domain.dto.LectureConverter.getLectureFromLectureDTO;

@Service
public class LectureService {

    @Autowired
    private LectureRepository lectureRepository;

    public List<CourseDTO> getCoursesByLecture(String lectureName) {

        Lecture lecture = findByName(lectureName);
        return getCourseDTOListFromCourseList(lecture.getCourses());
    }

    public Lecture create(LectureDTO lectureDTO) {

        Lecture newLecture = getLectureFromLectureDTO(lectureDTO);
        return save(newLecture);
    }

    public Lecture findBydId(String lectureId) {
        return lectureRepository.findOne(Long.valueOf(lectureId));
    }

    public Lecture findByName(String name) {
        return lectureRepository.findByName(name);
    }

    public List<CourseDTO> getCoursesFromTeacherAndLecture(String teacherId, String lecture) {

        Lecture oldLecture = findByName(lecture);
        List<CourseDTO> result;

        if (isTeacherInLecture(teacherId, oldLecture)) {
            result = getCourseDTOListFromCourseList(oldLecture.getCourses());
            return result;
        }

        return null;
    }

    public Lecture save(Lecture lecture) {
        return lectureRepository.save(lecture);
    }

    public Lecture update(Lecture lecture) {
        return save(lecture);
    }

    private List<CourseDTO> getCourseDTOListFromCourseList(List<Course> courses) {

        List<CourseDTO> courseDTOS = new ArrayList<>();
        for (int index = 0; index < courses.size(); index++) {
            Course course = courses.get(index);
            CourseDTO newCourseDto = getCourseDTOFromCourse(course);
            courseDTOS.add(newCourseDto);
        }
        return courseDTOS;
    }

    private boolean isTeacherInLecture(String teacherId, Lecture oldLecture) {

        List<String> participantsIds = oldLecture.getParticipantsIds();

        for (String participantId :
                participantsIds) {
            if (teacherId.equals(participantId)) {
                return true;
            }
        }
        return false;
    }
}
