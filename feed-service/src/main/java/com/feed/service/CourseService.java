package com.feed.service;

import com.feed.domain.Course;
import com.feed.domain.Lecture;
import com.feed.domain.dto.CourseDTO;
import com.feed.domain.dto.LectureDTO;
import com.feed.repository.CourseRepository;
import org.apache.commons.codec.binary.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.feed.domain.dto.CourseConverter.getCourseDTOFromCourse;
import static com.feed.domain.dto.CourseConverter.getCourseFromCourseDTO;

@Service
public class CourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private LectureService lectureService;

    private boolean teacherIsInLecture(String teacherId, List<String> participantsIds) {

        for (String participant :
                participantsIds) {
            if (participant.equals(teacherId)) {
                return true;
            }
        }
        return false;
    }

    public Course create(CourseDTO courseDTO, byte[] fileBytes, String contentType) {

        Course newCourse = getCourseFromCourseDTO(courseDTO);

        newCourse.setDocument(fileBytes);
        newCourse.setContentType(contentType);
        Lecture lecture = lectureService.findByName(courseDTO.getLectureName());
        if (lecture == null) {
            LectureDTO lectureDTO = new LectureDTO();
            lectureDTO.setName(courseDTO.getLectureName());
            lecture = lectureService.create(lectureDTO);
        }

        newCourse.setLecture(lecture);
        newCourse = save(newCourse);
        lecture.addCourse(newCourse);
        String teacherId = newCourse.getTeacherId();
        boolean participantIsSaved = lecture.getParticipantsIds().stream().noneMatch(s ->
                StringUtils.equals(s, teacherId));
        if (participantIsSaved) {
            lecture.addParticipant(teacherId);
        }
        lectureService.update(lecture);

        return newCourse;
    }

    public Course getCourse(Long id) {
        return courseRepository.findOne(id);
    }

    public Course getCourse(String teacherId, String lectureName, String courseName) {

        Lecture theLecture = lectureService.findByName(lectureName);
        if (teacherIsInLecture(teacherId, theLecture.getParticipantsIds())) {
            List<Course> courses = theLecture.getCourses();

            Course course = courses.stream()
                    .filter(elem -> Objects.equals(elem.getName(), courseName))
                    .collect(Collectors.toList()).get(0);

            return course;
        }
        return null;
    }

    public Course save(Course course) {

        return courseRepository.save(course);
    }

    public List<CourseDTO> getTeacherCourses(String teacherId) {

        List<Course> courses = courseRepository.findByTeacherIdOrderByIdDesc(teacherId);
        List<CourseDTO> result = new ArrayList<>();
        courses.forEach(course ->
                result.add(getCourseDTOFromCourse(course))
        );
        return result;
    }

    public CourseDTO getCourseContent(String id) {

        Course course = courseRepository.findOne(Long.valueOf(id));
        return getCourseDTOFromCourse(course);
    }
}