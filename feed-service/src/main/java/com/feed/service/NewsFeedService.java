package com.feed.service;

import com.feed.domain.Question;
import com.feed.domain.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class NewsFeedService {

    @Autowired
    private QuestionService questionService;

    @Autowired
    private StatusService statusService;

    public List<Object> getNewsFeed(){

        List<Question> questions = questionService.getAllWithAnswerOrdered();
        List<Status> statuses = statusService.getAllStatusesForNewsFeed();
        List<Object> result = new ArrayList<>();

        for (int questionIndex = 0; questionIndex < questions.size(); questionIndex++) {
            for (int statusIndex = 0; statusIndex < statuses.size(); statusIndex++) {
                if (questions.get(questionIndex).getPostingDate().getTime() > statuses.get(statusIndex).getPostingDate().getTime()) {
                    result.add(questions.get(questionIndex));
                    break;
                } else if (questionIndex + statusIndex >= result.size()) {
                    result.add(statuses.get(statusIndex));
                }
            }
            if (questionIndex == (questions.size() - 1 ) ) {
                for (int index = questionIndex + 1 ; index < statuses.size(); index++) {
                    result.add(statuses.get(index));
                }
            }
        }

        return result;
    }
}
