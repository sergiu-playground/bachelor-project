package com.feed.service;

import com.feed.domain.Question;
import com.feed.domain.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FeedService {

    @Autowired
    private QuestionService questionService;

    @Autowired
    private StatusService statusService;

    public List<Object> getTeacherFeed(String teacherId) {

        List<Question> questions = questionService.getAllByTeacherIdAndAnswerNotNull(teacherId);
        List<Status> statuses = statusService.getAllByUserId(Long.valueOf(teacherId));
        List<Object> result = new ArrayList<>();


        int actualAddedStatus = 0;
        for (int questionIndex = 0; questionIndex < questions.size(); questionIndex++) {
            for (int statusIndex = actualAddedStatus; statusIndex < statuses.size(); statusIndex++) {
                if (questions.get(questionIndex).getPostingDate().getTime() > statuses.get(statusIndex).getPostingDate().getTime()) {
                    result.add(questions.get(questionIndex));
                    break;
                } else {
                    result.add(statuses.get(statusIndex));
                    actualAddedStatus = statusIndex + 1;
                }
            }
            if (questionIndex == (questions.size() - 1 ) ) {
                for (int index = actualAddedStatus; index < statuses.size(); index++) {
                    result.add(statuses.get(index));
                }
            }
        }

        return result;
    }
}
