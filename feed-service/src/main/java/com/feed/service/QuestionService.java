package com.feed.service;

import com.feed.domain.Question;
import com.feed.domain.dto.QuestionDTO;
import com.feed.repository.QuestionRepository;
import com.feed.domain.dto.QuestionConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Transactional
public class QuestionService {

    @Autowired
    private QuestionRepository questionRepository;

    public Question create(QuestionDTO questionDTO) {

        Question question = QuestionConverter.getQuestionFromQuestionDTO(questionDTO);
        Question newQuestion = save(question);
        return newQuestion;
    }

    public List<Question> getAll() {
        return questionRepository.findAll();
    }

    public List<Question> getAllWithAnswerOrdered() {

        List<Question> result = questionRepository.findAllByAnswerNotNullOrderByIdDesc();
        return result;
    }

    public List<Question> getAllByTeacherIdAndAnswerNotNull(String teacherId) {

        List<Question> result = questionRepository.findAllByAnswererIdAndAnswerNotNullOrderByIdDesc(teacherId) ;
        return result;
    }

    public List<Question> getAllByTeacherIdAndAnswerNull(String teacherId) {

        List<Question> result = questionRepository.findAllByAnswererIdAndAnswerNullOrderByIdDesc(teacherId);
        return result;
    }

    public Question save(Question question) {
        return questionRepository.save(question);
    }

    private List<Question> getFilteredQuestionDTOS(String teacherId, List<Question> allQuestions, Boolean isNull) {

        List<Question> filteredQuestions = allQuestions.stream()
                .filter(question -> Objects.equals(question.getAnswererId(), teacherId))
                .filter(question -> Objects.isNull(question.getAnswer()) == isNull)
                .collect(Collectors.toList());

        List<QuestionDTO> result = new ArrayList<>();
        filteredQuestions.forEach(question -> {
            QuestionDTO questionDTO = new QuestionDTO();
            questionDTO.setQuestion(question.getQuestion());
            questionDTO.setAnswer(question.getAnswer());
            result.add(questionDTO);
        });
        return filteredQuestions;
    }

    public void answerQuestion(Question question) {

        Question oldQuestion = questionRepository.findOne(question.getId());
        oldQuestion.setAnswer(question.getAnswer());
        save(oldQuestion);
    }
}