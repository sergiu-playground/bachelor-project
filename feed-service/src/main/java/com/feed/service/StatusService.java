package com.feed.service;

import com.feed.domain.Status;
import com.feed.domain.dto.StatusDTO;
import com.feed.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

import static com.feed.domain.dto.StatusConverter.getStatusFromStatusDTO;

@Service
@Transactional
public class StatusService {

    @Autowired
    private StatusRepository statusRepository;

    public Status create(StatusDTO statusDTO) {

        Status newStatus = getStatusFromStatusDTO(statusDTO);
        return save(newStatus);
    }

    private Status save(Status newStatus) {
        return statusRepository.save(newStatus);
    }

    public List<Status> getAll() {
        return statusRepository.findAll();
    }

    public List<Status> getAllByUserId(final Long id) {

        List<Status> statuses = statusRepository.findAllByUserIdOrderByIdDesc(id.toString());
        return statuses;
    }

    public List<Status> getAllStatusesForNewsFeed() {

        List<Status>statuses = statusRepository.findAllByOrderByIdDesc();
        return statuses;
    }

    public List<Status> getAllStatusesFromTeachers() {

        List<Status> statuses = statusRepository.findAll();
        return statuses;
    }
}