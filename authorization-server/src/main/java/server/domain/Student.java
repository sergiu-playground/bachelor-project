package server.domain;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Student")
public class Student extends User {

    @Column(name = "student_group")
    private String group;

    @Column(name = "student_year")
    private String year;

    @Column(name = "student_semester")
    private String semester;

    @ElementCollection
    private List<Long> courseId;

    @ManyToMany(
            fetch = FetchType.EAGER)
    @JoinTable(
            name = "teacher_student",
            joinColumns = @JoinColumn(
                    name = "student_id",
                    referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "teacher_id",
                    referencedColumnName = "id"))
    private List<Teacher> teachers;

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
    }

    public List<Long> getCourseId() {
        return courseId;
    }

    public void setCourseId(List<Long> courseId) {
        this.courseId = courseId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Student student = (Student) o;
        return Objects.equals(group, student.group) &&
                Objects.equals(year, student.year) &&
                Objects.equals(semester, student.semester) &&
                Objects.equals(courseId, student.courseId) &&
                Objects.equals(teachers, student.teachers);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), group, year, semester, courseId, teachers);
    }
}
