package server.domain;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "teacher")
public class Teacher extends User {

    @ElementCollection
    private List<Long> coursesId;

    @Column(name = "teacher_year")
    private String year;

    @Column(name = "teacher_semester")
    private String semester;

    @ManyToMany(
            fetch = FetchType.EAGER)
    @JoinTable(
            name = "teacher_student",
            joinColumns = @JoinColumn(
                    name = "teacher_id",
                    referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "student_id",
                    referencedColumnName = "id"))
    private List<Student> students;

    public List<Long> getCoursesId() {
        return coursesId;
    }

    public void setCoursesId(List<Long> coursesId) {
        this.coursesId = coursesId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Teacher teacher = (Teacher) o;
        return Objects.equals(coursesId, teacher.coursesId) &&
                Objects.equals(year, teacher.year) &&
                Objects.equals(semester, teacher.semester) &&
                Objects.equals(students, teacher.students);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), coursesId, year, semester, students);
    }
}
