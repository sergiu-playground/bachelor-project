package server.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class PrincipalService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PrincipalService.class);

    @RequestMapping(value = "/principal")
    public Principal principal(Principal principal) {
        LOGGER.info("Principal: {}", principal);
        return principal;
    }
}
