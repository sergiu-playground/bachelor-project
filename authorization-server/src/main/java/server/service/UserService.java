package server.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.domain.User;
import server.repository.UserRepository;


/***
 * Service for handling {@link User}s and authentication.
 */
@Service
public class UserService{

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    /***
     *  Save User in the database.
     *
     * @param user User to save
     * @return saved {@link User}
     */
    public User save(final User user) {
        LOGGER.info("Trying to save user with email={}", user.getEmail());
        User saveUser = userRepository.save(user);
        LOGGER.info("Successfully saved user with email={}", user.getEmail());
        return saveUser;
    }

    /***
     *  Search for a user in database.
     *
     * @param email User email
     * @return a {@link User} if found or <code>null</code> otherwise
     */
    public User getUserByEmail(final String email) {
        return userRepository.findByEmail(email);
    }
}
