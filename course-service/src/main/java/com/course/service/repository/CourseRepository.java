package com.course.service.repository;

import com.course.service.domain.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
    List<Course> findByTeacherIdOrderByIdDesc(@RequestParam(name = "teacherId") String teacherId);
}
