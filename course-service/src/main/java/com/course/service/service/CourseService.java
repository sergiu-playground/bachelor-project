package com.course.service.service;

import com.course.service.domain.Course;
import com.course.service.domain.Lecture;
import com.course.service.domain.dto.CourseDTO;
import com.course.service.domain.dto.LectureDTO;
import com.course.service.repository.CourseRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.course.service.domain.dto.CourseConverter.*;

@Service
@Transactional
public class CourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private LectureService lectureService;

    public Course create(CourseDTO courseDTO, byte[] fileBytes, String contentType) {

        Course newCourse = getCourseFromCourseDTO(courseDTO);

        newCourse.setDocument(fileBytes);
        newCourse.setContentType(contentType);
        Lecture lecture = lectureService.findByName(courseDTO.getLectureName());
        if (lecture == null) {
            LectureDTO lectureDTO = new LectureDTO();
            lectureDTO.setName(courseDTO.getLectureName());
            lecture = lectureService.create(lectureDTO);
            lecture.addParticipant(newCourse.getTeacherId());
        }

        newCourse.setLecture(lecture);
        newCourse = save(newCourse);
        lecture.addCourse(newCourse);
        String teacherId = newCourse.getTeacherId();
        boolean participantIsSaved = lecture.getParticipantsIds().stream().noneMatch(s ->
                StringUtils.equals(s, teacherId));
        if (participantIsSaved) {
            lecture.addParticipant(teacherId);
        }
        lectureService.update(lecture);

        return newCourse;
    }

    public Course save(Course course) {

        return courseRepository.save(course);
    }

    public Course getCourse(String teacherId, String lectureName, String courseName) throws Exception {
        Lecture theLecture = lectureService.findByName(lectureName);
        if (teacherIsInLecture(teacherId, theLecture.getParticipantsIds())) {

            List<Course> courses = theLecture.getCourses();

            Course course = courses.stream()
                    .filter(elem -> Objects.equals(elem.getName(), courseName))
                    .collect(Collectors.toList()).get(0);

            return course;
        }

        throw new Exception("The course with name " + courseName + " is not saved in database.");

    }

    private boolean teacherIsInLecture(String teacherId, List<String> participantsIds) {

        for (String participant :
                participantsIds) {
            if (participant.equals(teacherId)) {
                return true;
            }
        }
        return false;
    }

    public List<CourseDTO> getTeacherCourses(String teacherId) {
        List<Course> courses = courseRepository.findByTeacherIdOrderByIdDesc(teacherId);
        List<CourseDTO> result = new ArrayList<>();

        courses.forEach(course ->
                result.add(getLightVersionCourseDTOFromCourse(course))
        );
        return result;
    }

    public CourseDTO getCourseContent(String id) {
        Course course = courseRepository.findOne(Long.valueOf(id));
        return getCourseDTOFromCourse(course);
    }

    public void update(String id, CourseDTO courseDTO, byte[] file, String contentType) {

        Course oldCourse = getCourse(Long.valueOf(id));

        if (StringUtils.isNotBlank(courseDTO.getCourseName())) {
            oldCourse.setName(courseDTO.getCourseName());
        }
        if (StringUtils.isNotBlank(courseDTO.getLectureName())) {
            Lecture newLecture = lectureService.findByName(courseDTO.getLectureName());
            oldCourse.setLecture(newLecture);
        }
        if (StringUtils.isNotBlank(contentType) && file.length > 0) {
            oldCourse.setDocument(file);
            oldCourse.setContentType(contentType);
        }
        save(oldCourse);
    }

    public Course getCourse(Long id) {
        return courseRepository.findOne(id);
    }
}
