package com.course.service.service;

import com.course.service.domain.Course;
import com.course.service.domain.Lecture;
import com.course.service.domain.dto.CourseDTO;
import com.course.service.domain.dto.LectureDTO;
import com.course.service.repository.LectureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.course.service.domain.dto.CourseConverter.getCourseDTOFromCourse;
import static com.course.service.domain.dto.LectureConverter.getLectureFromLectureDTO;

@Service
public class LectureService {

    @Autowired
    private LectureRepository lectureRepository;

    public Lecture create(LectureDTO lectureDTO) {
        Lecture newLecture = getLectureFromLectureDTO(lectureDTO);

        return save(newLecture);
    }

    public Lecture save(Lecture lecture) {
        return lectureRepository.save(lecture);
    }

    public Lecture findBydId(String lectureId) {
        return lectureRepository.findOne(Long.valueOf(lectureId));
    }

    public List<CourseDTO> getCoursesByLecture(String lectureName) {
        Lecture lecture = findByName(lectureName);

        return getCourseDTOListFromCourseList(lecture.getCourses());
    }

    public Lecture findByName(String name) {
        return lectureRepository.findByName(name);
    }

    private List<CourseDTO> getCourseDTOListFromCourseList(List<Course> courses) {

        List<CourseDTO> courseDTOS = new ArrayList<>();

        for (int index = 0; index < courses.size(); index++) {
            Course course = courses.get(index);
            CourseDTO newCourseDto = getCourseDTOFromCourse(course);
            courseDTOS.add(newCourseDto);
        }
        return courseDTOS;
    }

    public List<CourseDTO> getCoursesFromTeacherAndLecture(String teacherId, String lecture) {
        Lecture oldLecture = findByName(lecture);
        List<CourseDTO> result;

        if (isTeacherInLecture(teacherId, oldLecture)) {
            result = getCourseDTOListFromCourseList(oldLecture.getCourses());
            return result;
        }

        throw new RuntimeException("The teacher is not registered.");

    }

    private boolean isTeacherInLecture(String teacherId, Lecture oldLecture) {

        List<String> participantsIds = oldLecture.getParticipantsIds();

        for (String participantId :
                participantsIds) {
            if (teacherId.equals(participantId)) {
                return true;
            }
        }
        return false;
    }

    public Lecture update(Lecture lecture) {
        return save(lecture);
    }

    public List<LectureDTO> findLecturesByTeacherId(String teacherId) {
        List<Lecture> lectures = findAll();
        List<LectureDTO> result = new ArrayList<>();

        List<Lecture> goodLectures = lectures.stream().filter(l ->
                l.getParticipantsIds().contains(teacherId)
        ).collect(Collectors.toList());

        goodLectures.forEach(lecture ->
        {
            LectureDTO lectureDTO = new LectureDTO();
            lectureDTO.setName(lecture.getName());
            result.add(lectureDTO);
        });

        return result;
    }

    public List<Lecture> findAll() {
        return lectureRepository.findAll();
    }
}
