package com.course.service.controller;

import com.course.service.domain.Lecture;
import com.course.service.domain.dto.CourseDTO;
import com.course.service.domain.dto.LectureDTO;
import com.course.service.service.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/lecture")
public class LectureController {

    @Autowired
    private LectureService lectureService;

    @RequestMapping(value = "/{lecture}", method = RequestMethod.GET)
    public ResponseEntity getCourses(@PathVariable("lecture") String lectureName) {
        List<CourseDTO> courses = lectureService.getCoursesByLecture(lectureName);

        return new ResponseEntity(courses, HttpStatus.OK);
    }

    @RequestMapping(value = "/getCoursesBy/{teacherId}/{lecture}", method = RequestMethod.GET)
    public ResponseEntity getCoursesFromATeacherAndLecture(@PathVariable("teacherId") String teacherId,
                                                           @PathVariable("lecture") String lecture) {

        List<CourseDTO> courses = lectureService.getCoursesFromTeacherAndLecture(teacherId, lecture);

        return new ResponseEntity(courses, HttpStatus.OK);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ResponseEntity save(@RequestBody LectureDTO lectureDTO) {

        Lecture responseLecture = lectureService.create(lectureDTO);

        return new ResponseEntity(responseLecture, HttpStatus.OK);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ResponseEntity getAll() {

        List<Lecture> responseLecture = lectureService.findAll();

        return new ResponseEntity(responseLecture, HttpStatus.OK);
    }

    @RequestMapping(value = "/getLecturesByTeacherId/{teacherId}", method = RequestMethod.GET)
    public ResponseEntity getLecturesByTeacherId(@PathVariable("teacherId") String teacherId) {

        List<LectureDTO> responseLecture = lectureService.findLecturesByTeacherId(teacherId);

        return new ResponseEntity(responseLecture, HttpStatus.OK);
    }
}
