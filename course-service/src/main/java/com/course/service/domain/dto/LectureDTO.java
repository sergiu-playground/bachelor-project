package com.course.service.domain.dto;

import java.io.Serializable;

public class LectureDTO implements Serializable {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
