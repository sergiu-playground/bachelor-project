package com.course.service.domain.dto;

import com.course.service.domain.Course;

public final class CourseConverter {

    private CourseConverter() {}

    public static Course getCourseFromCourseDTO(final CourseDTO courseDTO) {
        Course course = new Course();
        course.setTeacherId(courseDTO.getTeacherId());
        course.setName(courseDTO.getCourseName());
        return course;
    }

    public static CourseDTO getCourseDTOFromCourse(final Course course) {
        CourseDTO courseDTO = new CourseDTO();
        courseDTO.setLectureName(course.getLecture().getName());
        courseDTO.setTeacherId(course.getTeacherId());
        courseDTO.setCourseName(course.getName());
        courseDTO.setId(course.getId().toString());
        courseDTO.setContentType(course.getContentType());
        courseDTO.setDocument(course.getDocument());

        return courseDTO;
    }

    public static CourseDTO getLightVersionCourseDTOFromCourse(final Course course) {
        CourseDTO courseDTO = new CourseDTO();
        courseDTO.setId(course.getId().toString());
        courseDTO.setLectureName(course.getLecture().getName());
        courseDTO.setCourseName(course.getName());
        return courseDTO;
    }
}