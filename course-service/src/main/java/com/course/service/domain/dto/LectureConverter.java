package com.course.service.domain.dto;

import com.course.service.domain.Lecture;

public final class LectureConverter {

    private LectureConverter() {}
    public static Lecture getLectureFromLectureDTO(final LectureDTO lectureDTO) {

        Lecture newLecture = new Lecture();

        newLecture.setName(lectureDTO.getName());

        return newLecture;
    }
}