package com.course.service.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "lecture")
public class Lecture {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @OneToMany(fetch = FetchType.EAGER, targetEntity = Course.class)
    private List<Course> courses;

    /***
     * Participants can be teachers or students.
     */
    @ElementCollection
    private List<String> participantsIds;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public List<String> getParticipantsIds() {
        return participantsIds;
    }

    public void setParticipantsIds(List<String> participantsIds) {
        this.participantsIds = participantsIds;
    }

    public List<Course> addCourse(Course course) {
        if (this.courses == null) {
            this.courses = new ArrayList<>();
            this.courses.add(course);
            return courses;
        }
        this.courses.add(course);
        return courses;

    }

    public List<String> addParticipant(String userId) {
        if (this.participantsIds == null) {
            this.participantsIds = new ArrayList<>();
            this.participantsIds.add(userId);
            return this.participantsIds;
        }
        this.participantsIds.add(userId);
        return participantsIds;
    }
}
